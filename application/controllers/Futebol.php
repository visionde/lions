<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Futebol extends CI_Controller {

      private $modulo = "futebol";
      private $perfil = "FUTEBOL";

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Futebol_model');
		$this->load->library('session');
	}

     public function index()
	{

            $data['dadoselencoGoleira']  = $this->Futebol_model->consultaElencoGoleira();
            $elenco = $data['dadoselencoGoleira'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoGoleira'] = $elencofc;

            $data['dadoselencoDefensoras']  = $this->Futebol_model->consultaElencoDefensoras();
            $elenco = $data['dadoselencoDefensoras'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoDefensoras'] = $elencofc;

            $data['dadoselencoMeia']  = $this->Futebol_model->consultaElencoMeia();
            $elenco = $data['dadoselencoMeia'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoMeia'] = $elencofc;


            $data['dadoselencoAtacante']  = $this->Futebol_model->consultaElencoAtacante();
            $elenco = $data['dadoselencoAtacante'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoAtacante'] = $elencofc;


            $data['dadoscomissao']  = $this->Futebol_model->consultaComissao();
            $comissao = $data['dadoscomissao'] ;
            $comissaofc = json_decode(json_encode($comissao), True);
            $data['dadoscomissao'] = $comissaofc;

			$data['meio']   = 'futebol';
			$this->load->view('layout/layout',$data);

	}

  public function futebolm()
  {

            $data['dadoselencoGoleiro']  = $this->Futebol_model->consultaElencoGoleiro();
            $elenco = $data['dadoselencoGoleiro'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoGoleiro'] = $elencofc;

            $data['dadoselencoDefensores']  = $this->Futebol_model->consultaElencoDefensores();
            $elenco = $data['dadoselencoDefensores'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoDefensores'] = $elencofc;

            $data['dadoselencoMeia']  = $this->Futebol_model->consultaElencoMeiaM();
            $elenco = $data['dadoselencoMeia'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoMeia'] = $elencofc;


            $data['dadoselencoAtacante']  = $this->Futebol_model->consultaElencoAtacanteM();
            $elenco = $data['dadoselencoAtacante'] ;
            $elencofc = json_decode(json_encode($elenco), True);
            $data['dadoselencoAtacante'] = $elencofc;


            $data['dadoscomissao']  = $this->Futebol_model->consultaComissaoM();
            $comissao = $data['dadoscomissao'] ;
            $comissaofc = json_decode(json_encode($comissao), True);
            $data['dadoscomissao'] = $comissaofc;

      $data['meio']   = 'futebolm';
      $this->load->view('layout/layout',$data);

  }

      public function futeboladm($page = 1, $pesquisa = NULL)
      {

            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

        $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
        $atletas = $data['contadorAtletas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletas'] = $atletasfc;

        $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
        $atletas = $data['contadorComissao'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissao'] = $atletasfc;

        $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
        $atletas = $data['contadorComissaoAntiga'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissaoAntiga'] = $atletasfc;

        $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
        $atletas = $data['contadorAtletasAntigas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletasAntigas'] = $atletasfc;

        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 15;
        $data['dados']  = $this->Futebol_model->listarAtletas($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'futebol/futeboladm';
        $total = $this->Futebol_model->contadorAtletas($page, $pesquisa);

        if ($total){
            $data['total']   = $total[0]->total;
        }else{
            $data['total']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
            $data['itens_per_page'] = $itens_per_page;

        $this->load->view('layout/layout',$data);

      }

     public function adicionarAtleta()
      {

            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }
            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }


        $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
        $atletas = $data['contadorAtletas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletas'] = $atletasfc;

        $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
        $atletas = $data['contadorComissao'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissao'] = $atletasfc;

        $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
        $atletas = $data['contadorComissaoAntiga'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissaoAntiga'] = $atletasfc;

        $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
        $atletas = $data['contadorAtletasAntigas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletasAntigas'] = $atletasfc;


        $data['meio']   = 'futebol/adicionarAtleta';
      
        $this->load->view('layout/layout',$data);

      }

     public function adicionarAtletaM()
      {

            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }
            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }


        $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
        $atletas = $data['contadorAtletas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletas'] = $atletasfc;

        $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
        $atletas = $data['contadorComissao'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissao'] = $atletasfc;

        $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
        $atletas = $data['contadorComissaoAntiga'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissaoAntiga'] = $atletasfc;

        $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
        $atletas = $data['contadorAtletasAntigas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletasAntigas'] = $atletasfc;


        $data['meio']   = 'futebol/adicionarAtletaM';
      
        $this->load->view('layout/layout',$data);

      }

     public function adicionarAtletaExe()
     {

            if(!verificarPermissao($this->modulo)){

                      redirect('permissao');            

                  }
            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }
            
        $this->form_validation->set_rules('nome','Nome','trim|required');
        // $this->form_validation->set_rules('apelido','Apelido','trim|required');
        $this->form_validation->set_rules('posicao','Posição','trim|required');
        // $this->form_validation->set_rules('cpf','CPF','trim|required');
        // $this->form_validation->set_rules('rg','RG','trim|required'); 
        $this->form_validation->set_rules('data','Data de Nascimento','trim|required');
        // $this->form_validation->set_rules('celular','CELULAR','trim|required');
        // $this->form_validation->set_rules('nomem','Nome da Mâe','trim|required');
        // $this->form_validation->set_rules('nomep','Nome do Pai','trim|required');
        // $this->form_validation->set_rules('endereco','Endereço','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
            if ($this->input->post('time') == 'F') {
                $this->adicionarAtleta();
            }else{
                $this->adicionarAtletaM();
            }
                
        }else{

        $foto_ok = 'off';
        $foto_doc_ok = 'off';       
        $arrDadosArquivo["type"]      = $_FILES["foto"]["type"];
        $arrDadosArquivo2["type"]     = $_FILES["foto_doc"]["type"];
        $arquivo                      = $_FILES['foto']['tmp_name'];
        $arquivo2                     = $_FILES['foto_doc']['tmp_name'];

              if($arquivo){

                    $separador = explode('/', $arrDadosArquivo["type"]);
                    $separador = isset($separador[1]) ? $separador[1] : '';

                                if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto invalida. Tente Novamente!');
                                         $this->adicionarAtleta();
                                 }
                  $foto_ok = 'ok';


              }

              if($arquivo2){

                    $separador2 = explode('/', $arrDadosArquivo2["type"]);
                    $separador2 = isset($separador2[1]) ? $separador2[1] : '';
                              if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto Documento invalida. Tente Novamente!');
                                         $this->adicionarAtleta();
                                 }
                    $foto_doc_ok = 'ok';

              }

              if($foto_ok == 'ok'){

                 $foto = date('dmYhis') . '.' . 'jpeg';
                 $configuracao = array(
                     'upload_path'   => './assents/arquivos/fotos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto,
                     'max_size'      => '50000000000'
                 );      
                 $this->load->library('upload');
                 $this->upload->initialize($configuracao);
                 if ($this->upload->do_upload('foto'))
                     $destaque = true;
                 else
                     echo $this->upload->display_errors();
               }
               if($foto_doc_ok =='ok') {

                  $foto_doc     = date('dmYhis') . '.' . 'jpeg';
                  $configuracao2 = array(
                     'upload_path'   => './assents/arquivos/documentos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto_doc,
                     'max_size'      => '5000000000000'
                  );  
                  $this->load->library('upload');
                  $this->upload->initialize($configuracao2);
                  if ($this->upload->do_upload('foto_doc'))
                     $mini =  true;
                  else
                     echo $this->upload->display_errors();
               }

            $nome              = $this->input->post('nome');
            $apelido           = $this->input->post('apelido');
            $posicao           = $this->input->post('posicao');
            $cpf               = $this->input->post('cpf');
            $rg                = $this->input->post('rg');
            $data              = $this->input->post('data');
            $celular           = $this->input->post('celular');
            $nomem             = $this->input->post('nomem');
            $nomep             = $this->input->post('nomep');
            $endereco          = $this->input->post('endereco');
            $time              = $this->input->post('time');
            $status            = 'Atleta';
       if($foto_ok =='ok'){
            $foto1             = $foto;
       }else{
          
          if ($time == 'F') {
                      if ($time == 'F') {
            $foto1             = 'padrao.png';
          }else{
            $foto1             = 'padraom.jpg';
          }
          }else{
            $foto1             = 'padraom.jpg';
          }
            
       }

       if($foto_doc_ok =='ok'){
            $foto2             = $foto_doc;
       }else{
            $foto2             = '';

       }            
            
            
            $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto'             =>        $foto1,
                              'foto_doc'         =>        $foto2,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->adicionarAtletaExe($dados);
            
            if($resultado){
                  $this->session->set_flashdata('success','Atleta adicionarda com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao inserir Atleta!');
            }

            redirect('futebol/futeboladm'); 


        }

     }

   public function editarAtleta($id,$time)
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }
            
            $dados  = array(
                        'id' => $id
                  );
                  
            $resultado = $this->Futebol_model->editarAtleta($dados);  

              $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
              $atletas = $data['contadorAtletas'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorAtletas'] = $atletasfc;

              $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
              $atletas = $data['contadorComissao'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorComissao'] = $atletasfc;

              $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
              $atletas = $data['contadorComissaoAntiga'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorComissaoAntiga'] = $atletasfc;

              $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
              $atletas = $data['contadorAtletasAntigas'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorAtletasAntigas'] = $atletasfc;
            

            if ($time == 'F') {
              $data['meio']   =  'futebol/editarAtleta';
            }else{
              $data['meio']   =  'futebol/editarAtletaM';
            }
                     
            $data['dados']  =  $resultado[0];
            $this->load->view('layout/layout',$data);
     
      }

    public function editarAtletaExe()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

            $id = $this->input->post('id');
            
            $dados  = array(
                        'id' => $id
                  );
                  
            $this->form_validation->set_rules('nome','Nome','trim|required');
        // $this->form_validation->set_rules('apelido','Apelido','trim|required');
        $this->form_validation->set_rules('posicao','Posição','trim|required');
        // $this->form_validation->set_rules('cpf','CPF','trim|required');
        // $this->form_validation->set_rules('rg','RG','trim|required'); 
        $this->form_validation->set_rules('data','Data de Nascimento','trim|required');
        // $this->form_validation->set_rules('celular','CELULAR','trim|required');
        // $this->form_validation->set_rules('nomem','Nome da Mâe','trim|required');
        // $this->form_validation->set_rules('nomep','Nome do Pai','trim|required');
        // $this->form_validation->set_rules('endereco','Endereço','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editarAtleta();
        }else{

        $foto_ok = 'off';
        $foto_doc_ok = 'off';       
        $arrDadosArquivo["type"]      = $_FILES["foto"]["type"];
        $arrDadosArquivo2["type"]     = $_FILES["foto_doc"]["type"];
        $arquivo                      = $_FILES['foto']['tmp_name'];
        $arquivo2                     = $_FILES['foto_doc']['tmp_name'];

              if($arquivo){

                    $separador = explode('/', $arrDadosArquivo["type"]);
                    $separador = isset($separador[1]) ? $separador[1] : '';

                                if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto invalida. Tente Novamente!');
                                         $this->editarAtleta();
                                 }
                  $foto_ok = 'ok';


              }

              if($arquivo2){

                    $separador2 = explode('/', $arrDadosArquivo2["type"]);
                    $separador2 = isset($separador2[1]) ? $separador2[1] : '';
                              if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto Documento invalida. Tente Novamente!');
                                         $this->editarAtleta();
                                 }
                    $foto_doc_ok = 'ok';

              }

              if($foto_ok == 'ok'){

                 $foto = date('dmYhis') . '.' . 'jpeg';
                 $configuracao = array(
                     'upload_path'   => './assents/arquivos/fotos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto,
                     'max_size'      => '50000000000'
                 );      
                 $this->load->library('upload');
                 $this->upload->initialize($configuracao);
                 if ($this->upload->do_upload('foto'))
                     $destaque = true;
                 else
                     echo $this->upload->display_errors();
               }
               if($foto_doc_ok =='ok') {

                  $foto_doc     = date('dmYhis') . '.' . 'jpeg';
                  $configuracao2 = array(
                     'upload_path'   => './assents/arquivos/documentos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto_doc,
                     'max_size'      => '5000000000000'
                  );  
                  $this->load->library('upload');
                  $this->upload->initialize($configuracao2);
                  if ($this->upload->do_upload('foto_doc'))
                     $mini =  true;
                  else
                     echo $this->upload->display_errors();
               }

            $nome              = $this->input->post('nome');
            $apelido           = $this->input->post('apelido');
            $posicao           = $this->input->post('posicao');
            $cpf               = $this->input->post('cpf');
            $rg                = $this->input->post('rg');
            $data              = $this->input->post('data');
            $celular           = $this->input->post('celular');
            $nomem             = $this->input->post('nomem');
            $nomep             = $this->input->post('nomep');
            $endereco          = $this->input->post('endereco');
            $time              = $this->input->post('time');
            $status            = 'Atleta';


            if ($foto_doc_ok =='ok') {

                 $foto2 = $foto_doc ;
            }


            if ($foto_ok =='ok') {
                 
                 $foto1 = $foto ;
            }


      if ($foto_doc_ok =='ok' and $foto_ok =='ok') {
             $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto'             =>        $foto1,
                              'foto_doc'         =>        $foto2,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarAtletaExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Atleta editada com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Atleta!');
            }

            redirect('futebol/futeboladm'); 
      }elseif($foto_ok =='ok' and $foto_doc_ok !='ok'){
                         $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto'             =>        $foto1,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarAtletaExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Atleta editada com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Atleta!');
            }

            redirect('futebol/futeboladm'); 
      }elseif($foto_ok !='ok' and $foto_doc_ok =='ok'){
                   $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto_doc'         =>        $foto2,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarAtletaExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Atleta editada com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Atleta!');
            }

            redirect('futebol/futeboladm'); 
     

        }else{
                 $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarAtletaExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Atleta editada com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Atleta!');
            }

            redirect('futebol/futeboladm'); 

        }
     
     }

  }

  public function excluirAtleta($id)
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 

        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }        

        $id = $this->uri->segment(3);

        $dados  = array(
                                'visivel' => 0
                            );

        $resultado = $this->Futebol_model->excluirAtleta($dados,$id);

        if($resultado){
              $this->session->set_flashdata('success','Atleta excluida com sucesso!');
        }else{
              $this->session->set_flashdata('erro','Erro ao excluir!');
        }

        redirect('futebol/futeboladm','refresh');          

  }

  public function comissao($page = 1, $pesquisa = NULL)
  {

        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        }

        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

    $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
    $atletas = $data['contadorAtletas'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorAtletas'] = $atletasfc;

    $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
    $atletas = $data['contadorComissao'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorComissao'] = $atletasfc;

    $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
    $atletas = $data['contadorComissaoAntiga'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorComissaoAntiga'] = $atletasfc;

    $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
    $atletas = $data['contadorAtletasAntigas'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorAtletasAntigas'] = $atletasfc;

    $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
    $itens_per_page = 15;
    $data['dados']  = $this->Futebol_model->listarComissao($page, $itens_per_page, $pesquisa);
    $data['meio']   = 'futebol/comissao';
    $total = $this->Futebol_model->contadorComissao($page, $pesquisa);

    if ($total){
        $data['total']   = $total[0]->total;
    }else{
        $data['total']   = null;
    }
    $data['page']   = $page;
    $data['pesquisa']   = $pesquisa;
        $data['itens_per_page'] = $itens_per_page;

    $this->load->view('layout/layout',$data);

  }


  public function comissaoAntiga($page = 1, $pesquisa = NULL)
  {

        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        }

        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

    $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
    $atletas = $data['contadorAtletas'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorAtletas'] = $atletasfc;

    $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
    $atletas = $data['contadorComissao'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorComissao'] = $atletasfc;

    $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
    $atletas = $data['contadorComissaoAntiga'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorComissaoAntiga'] = $atletasfc;

    $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
    $atletas = $data['contadorAtletasAntigas'] ;
    $atletasfc = json_decode(json_encode($atletas), True);
    $data['contadorAtletasAntigas'] = $atletasfc;

    $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
    $itens_per_page = 15;
    $data['dados']  = $this->Futebol_model->listarComissaoAntiga($page, $itens_per_page, $pesquisa);
    $data['meio']   = 'futebol/comissaoAntiga';
    $total = $this->Futebol_model->contadorComissaoAntiga($page, $pesquisa);

    if ($total){
        $data['total']   = $total[0]->total;
    }else{
        $data['total']   = null;
    }
    $data['page']   = $page;
    $data['pesquisa']   = $pesquisa;
        $data['itens_per_page'] = $itens_per_page;

    $this->load->view('layout/layout',$data);

  }

  public function adicionarComissao()
    {

          if(!verificarPermissao($this->modulo)){

              redirect('permissao');            

          }

          if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }


      $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
      $atletas = $data['contadorAtletas'] ;
      $atletasfc = json_decode(json_encode($atletas), True);
      $data['contadorAtletas'] = $atletasfc;

      $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
      $atletas = $data['contadorComissao'] ;
      $atletasfc = json_decode(json_encode($atletas), True);
      $data['contadorComissao'] = $atletasfc;

      $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
      $atletas = $data['contadorComissaoAntiga'] ;
      $atletasfc = json_decode(json_encode($atletas), True);
      $data['contadorComissaoAntiga'] = $atletasfc;

      $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
      $atletas = $data['contadorAtletasAntigas'] ;
      $atletasfc = json_decode(json_encode($atletas), True);
      $data['contadorAtletasAntigas'] = $atletasfc;


      $data['meio']   = 'futebol/adicionarComissao';
    
      $this->load->view('layout/layout',$data);

    }

  public function adicionarComissaoExe()
     {

            if(!verificarPermissao($this->modulo)){

                      redirect('permissao');            

                  }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }
            
        $this->form_validation->set_rules('nome','Nome','trim|required');
        // $this->form_validation->set_rules('apelido','Apelido','trim|required');
        $this->form_validation->set_rules('posicao','Posição','trim|required');
        $this->form_validation->set_rules('time','Time','trim|required');
        // $this->form_validation->set_rules('cpf','CPF','trim|required');
        // $this->form_validation->set_rules('rg','RG','trim|required'); 
        $this->form_validation->set_rules('data','Data de Nascimento','trim|required');
        // $this->form_validation->set_rules('celular','CELULAR','trim|required');
        // $this->form_validation->set_rules('nomem','Nome da Mâe','trim|required');
        // $this->form_validation->set_rules('nomep','Nome do Pai','trim|required');
        // $this->form_validation->set_rules('endereco','Endereço','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->adicionarComissao();
        }else{

        $foto_ok = 'off';
        $foto_doc_ok = 'off';       
        $arrDadosArquivo["type"]      = $_FILES["foto"]["type"];
        $arrDadosArquivo2["type"]     = $_FILES["foto_doc"]["type"];
        $arquivo                      = $_FILES['foto']['tmp_name'];
        $arquivo2                     = $_FILES['foto_doc']['tmp_name'];

              if($arquivo){

                    $separador = explode('/', $arrDadosArquivo["type"]);
                    $separador = isset($separador[1]) ? $separador[1] : '';

                                if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto invalida. Tente Novamente!');
                                         $this->adicionarComissao();
                                 }
                  $foto_ok = 'ok';


              }

              if($arquivo2){

                    $separador2 = explode('/', $arrDadosArquivo2["type"]);
                    $separador2 = isset($separador2[1]) ? $separador2[1] : '';
                              if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto Documento invalida. Tente Novamente!');
                                         $this->adicionarComissao();
                                 }
                    $foto_doc_ok = 'ok';

              }

              if($foto_ok == 'ok'){

                 $foto = date('dmYhis') . '.' . 'jpeg';
                 $configuracao = array(
                     'upload_path'   => './assents/arquivos/fotos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto,
                     'max_size'      => '50000000000'
                 );      
                 $this->load->library('upload');
                 $this->upload->initialize($configuracao);
                 if ($this->upload->do_upload('foto'))
                     $destaque = true;
                 else
                     echo $this->upload->display_errors();
               }
               if($foto_doc_ok =='ok') {

                  $foto_doc     = date('dmYhis') . '.' . 'jpeg';
                  $configuracao2 = array(
                     'upload_path'   => './assents/arquivos/documentos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto_doc,
                     'max_size'      => '5000000000000'
                  );  
                  $this->load->library('upload');
                  $this->upload->initialize($configuracao2);
                  if ($this->upload->do_upload('foto_doc'))
                     $mini =  true;
                  else
                     echo $this->upload->display_errors();
               }

            $nome              = $this->input->post('nome');
            $apelido           = $this->input->post('apelido');
            $posicao           = $this->input->post('posicao');
            $time              = $this->input->post('time');
            $cpf               = $this->input->post('cpf');
            $rg                = $this->input->post('rg');
            $data              = $this->input->post('data');
            $celular           = $this->input->post('celular');
            $nomem             = $this->input->post('nomem');
            $nomep             = $this->input->post('nomep');
            $endereco          = $this->input->post('endereco');
            $time              = $this->input->post('time');
            $status            = 'Comissao';
       if($foto_ok =='ok'){
            $foto1             = $foto;
       }else{
          if ($time == 'F') {
            $foto1             = 'padrao.png';
          }else{
            $foto1             = 'padraom.jpg';
          }
       }

       if($foto_doc_ok =='ok'){
            $foto2             = $foto_doc;
       }else{
            $foto2             = '';

       }            
            
            
            $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto'             =>        $foto1,
                              'foto_doc'         =>        $foto2,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->adicionarComissaoExe($dados);
            
            if($resultado){
                  $this->session->set_flashdata('success','Funcionário adicionado com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao inserir Funcionário!');
            }

            redirect('futebol/comissao'); 


        }

     }

  public function editarComissao($id)
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }
            
            $dados  = array(
                        'id' => $id
                  );
                  
            $resultado = $this->Futebol_model->editarComissao($dados);  

              $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
              $atletas = $data['contadorAtletas'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorAtletas'] = $atletasfc;

              $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
              $atletas = $data['contadorComissao'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorComissao'] = $atletasfc;

              $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
              $atletas = $data['contadorComissaoAntiga'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorComissaoAntiga'] = $atletasfc;

              $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
              $atletas = $data['contadorAtletasAntigas'] ;
              $atletasfc = json_decode(json_encode($atletas), True);
              $data['contadorAtletasAntigas'] = $atletasfc;
            
                  
            $data['meio']   =  'futebol/editarComissao';
            $data['dados']  =  $resultado[0];
            $this->load->view('layout/layout',$data);
     
      }

    public function editarComissaoExe()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

            $id = $this->input->post('id');
            
            $dados  = array(
                        'id' => $id
                  );
                  
            $this->form_validation->set_rules('nome','Nome','trim|required');
        // $this->form_validation->set_rules('apelido','Apelido','trim|required');
        $this->form_validation->set_rules('posicao','Posição','trim|required');
        $this->form_validation->set_rules('time','Time','trim|required');
        // $this->form_validation->set_rules('cpf','CPF','trim|required');
        // $this->form_validation->set_rules('rg','RG','trim|required'); 
        $this->form_validation->set_rules('data','Data de Nascimento','trim|required');
        // $this->form_validation->set_rules('celular','CELULAR','trim|required');
        // $this->form_validation->set_rules('nomem','Nome da Mâe','trim|required');
        // $this->form_validation->set_rules('nomep','Nome do Pai','trim|required');
        // $this->form_validation->set_rules('endereco','Endereço','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editarComissao($id);
        }else{

        $foto_ok = 'off';
        $foto_doc_ok = 'off';       
        $arrDadosArquivo["type"]      = $_FILES["foto"]["type"];
        $arrDadosArquivo2["type"]     = $_FILES["foto_doc"]["type"];
        $arquivo                      = $_FILES['foto']['tmp_name'];
        $arquivo2                     = $_FILES['foto_doc']['tmp_name'];

              if($arquivo){

                    $separador = explode('/', $arrDadosArquivo["type"]);
                    $separador = isset($separador[1]) ? $separador[1] : '';

                                if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto invalida. Tente Novamente!');
                                         $this->editarComissao();
                                 }
                  $foto_ok = 'ok';


              }

              if($arquivo2){

                    $separador2 = explode('/', $arrDadosArquivo2["type"]);
                    $separador2 = isset($separador2[1]) ? $separador2[1] : '';
                              if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
                                   
                                     $this->session->set_flashdata('erro','Extenção Foto Documento invalida. Tente Novamente!');
                                         $this->editarComissao();
                                 }
                    $foto_doc_ok = 'ok';

              }

              if($foto_ok == 'ok'){

                 $foto = date('dmYhis') . '.' . 'jpeg';
                 $configuracao = array(
                     'upload_path'   => './assents/arquivos/fotos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto,
                     'max_size'      => '50000000000'
                 );      
                 $this->load->library('upload');
                 $this->upload->initialize($configuracao);
                 if ($this->upload->do_upload('foto'))
                     $destaque = true;
                 else
                     echo $this->upload->display_errors();
               }
               if($foto_doc_ok =='ok') {

                  $foto_doc     = date('dmYhis') . '.' . 'jpeg';
                  $configuracao2 = array(
                     'upload_path'   => './assents/arquivos/documentos',
                     'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
                     'file_name'     => $foto_doc,
                     'max_size'      => '5000000000000'
                  );  
                  $this->load->library('upload');
                  $this->upload->initialize($configuracao2);
                  if ($this->upload->do_upload('foto_doc'))
                     $mini =  true;
                  else
                     echo $this->upload->display_errors();
               }

            $nome              = $this->input->post('nome');
            $apelido           = $this->input->post('apelido');
            $posicao           = $this->input->post('posicao');
            $time              = $this->input->post('time');
            $cpf               = $this->input->post('cpf');
            $rg                = $this->input->post('rg');
            $data              = $this->input->post('data');
            $celular           = $this->input->post('celular');
            $nomem             = $this->input->post('nomem');
            $nomep             = $this->input->post('nomep');
            $endereco          = $this->input->post('endereco');
            $time              = $this->input->post('time');
            $status            = 'Comissao';


            if ($foto_doc_ok =='ok') {

                 $foto2 = $foto_doc ;
            }


            if ($foto_ok =='ok') {
                 
                 $foto1 = $foto ;
            }


      if ($foto_doc_ok =='ok' and $foto_ok =='ok') {
             $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto'             =>        $foto1,
                              'foto_doc'         =>        $foto2,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarComissaoExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Funcionário editado com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Funcionário!');
            }

            redirect('futebol/comissao'); 
      }elseif($foto_ok =='ok' and $foto_doc_ok !='ok'){
                         $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto'             =>        $foto1,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarComissaoExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Funcionário editado com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Funcionário!');
            }

            redirect('futebol/comissao'); 
      }elseif($foto_ok !='ok' and $foto_doc_ok =='ok'){
                   $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'foto_doc'         =>        $foto2,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarComissaoExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Funcionário editado com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Funcionário!');
            }

            redirect('futebol/comissao'); 
     

        }else{
                 $dados = array(
                              'nome'             =>        $nome,              
                              'apelido'          =>        $apelido,         
                              'posicao'          =>        $posicao, 
                              'time'             =>        $time, 
                              'cpf'              =>        $cpf,              
                              'rg'               =>        $rg,                
                              'data_nascimento'  =>        $data,       
                              'celular'          =>        $celular,      
                              'nomem'            =>        $nomem,      
                              'nomep'            =>        $nomep,           
                              'ende'             =>        $endereco,
                              'time'             =>        $time,
                              'status'           =>        $status

                  );

            // echo'<pre>'; var_dump($dados); exit();

            $resultado = $this->Futebol_model->editarComissaoExe($dados,$id);
            
            if($resultado){
                  $this->session->set_flashdata('success','Funcionário editado com sucesso!');
            }else{
                  $this->session->set_flashdata('erro','Erro ao editar Funcionário!');
            }

            redirect('futebol/comissao'); 

        }
     
     }

  }

 public function excluirComissao($id)
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 


        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }        

        $id = $this->uri->segment(3);

        $dados  = array(
                                'visivel' => 0
                            );

        $resultado = $this->Futebol_model->excluirComissao($dados,$id);

        if($resultado){
              $this->session->set_flashdata('success','Funcionário excluido com sucesso!');
        }else{
              $this->session->set_flashdata('erro','Erro ao excluir!');
        }

        redirect('futebol/comissao','refresh');          

  }

  public function AtletaAntigas($page = 1, $pesquisa = NULL)
      {

            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

        $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
        $atletas = $data['contadorAtletas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletas'] = $atletasfc;

        $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
        $atletas = $data['contadorComissao'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissao'] = $atletasfc;

        $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
        $atletas = $data['contadorComissaoAntiga'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorComissaoAntiga'] = $atletasfc;

        $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
        $atletas = $data['contadorAtletasAntigas'] ;
        $atletasfc = json_decode(json_encode($atletas), True);
        $data['contadorAtletasAntigas'] = $atletasfc;

        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 15;
        $data['dados']  = $this->Futebol_model->listarAtletaAntigas($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'futebol/AtletaAntigas';
        $total = $this->Futebol_model->contadorAtletaAntigas($page, $pesquisa);

        if ($total){
            $data['total']   = $total[0]->total;
        }else{
            $data['total']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
            $data['itens_per_page'] = $itens_per_page;

        $this->load->view('layout/layout',$data);

      }

 public function ativarAtletaAntiga($id)
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 


        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }        

        $id = $this->uri->segment(3);

        $dados  = array(
                                'visivel' => 1
                            );

        $resultado = $this->Futebol_model->ativarAtletaAntiga($dados,$id);

        if($resultado){
              $this->session->set_flashdata('success','Atleta Ativada com sucesso!');
        }else{
              $this->session->set_flashdata('erro','Erro ao ativar!');
        }

        redirect('futebol/AtletaAntigas','refresh');          

  }


 public function ativarComissaoAntiga($id)
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 


        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }        

        $id = $this->uri->segment(3);

        $dados  = array(
                                'visivel' => 1
                            );

        $resultado = $this->Futebol_model->ativarComissaoAntiga($dados,$id);

        if($resultado){
              $this->session->set_flashdata('success','Funcionário Ativado com sucesso!');
        }else{
              $this->session->set_flashdata('erro','Erro ao ativar!');
        }

        redirect('futebol/comissaoAntiga','refresh');          

  }


   public function vizualizarAtletaAntiga($id)
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 


        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }        

        $id = $this->uri->segment(3);

        $dados  = array(
                        'id' => $id
                  );


        $resultado = $this->Futebol_model->vizualizarAtletaAntiga($dados);


          $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
          $atletas = $data['contadorAtletas'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorAtletas'] = $atletasfc;

          $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
          $atletas = $data['contadorComissao'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorComissao'] = $atletasfc;

          $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
          $atletas = $data['contadorComissaoAntiga'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorComissaoAntiga'] = $atletasfc;

          $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
          $atletas = $data['contadorAtletasAntigas'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorAtletasAntigas'] = $atletasfc;
            
                  
          $data['meio']   =  'futebol/vizualizarAtletaAntiga';
          $data['dados']  =  $resultado[0];
          $this->load->view('layout/layout',$data);         

  }

  public function vizualizarComissaoAntiga($id)
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 


        if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }        

        $id = $this->uri->segment(3);

        $dados  = array(
                        'id' => $id
                  );


        $resultado = $this->Futebol_model->vizualizarComissaoAntiga($dados);


          $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
          $atletas = $data['contadorAtletas'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorAtletas'] = $atletasfc;

          $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
          $atletas = $data['contadorComissao'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorComissao'] = $atletasfc;

          $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
          $atletas = $data['contadorComissaoAntiga'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorComissaoAntiga'] = $atletasfc;

          $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
          $atletas = $data['contadorAtletasAntigas'] ;
          $atletasfc = json_decode(json_encode($atletas), True);
          $data['contadorAtletasAntigas'] = $atletasfc;
            
                  
          $data['meio']   =  'futebol/vizualizarComissaoAntiga';
          $data['dados']  =  $resultado[0];
          $this->load->view('layout/layout',$data);         

  }

   public function contrato($id){

        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        } 

        if(!Permissao($this->perfil)){

                 redirect('permissao');      

         } 

        $dados  = array(
            'id' => $id
        ); 

        $dadosView['dados'] = $this->Futebol_model->editarAtleta($dados); 
        
        list ($dia, $mes, $ano) = explode('/', $dadosView['dados'][0]->data_nascimento);

        $dadosView['ano'] = $ano;
        $dadosView['mes'] = $mes;
        $dadosView['dia'] = $dia;

        $dadosView['meio']     = 'futebol/contrato';
        $html = $this->load->view('layout/impressao',$dadosView, true); 
        
        gerarPDF($html);


       }


    public function baixar()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }


            $data['contadorAtletas'] = $this->Futebol_model->contadorAtletasMenu();
            $atletas = $data['contadorAtletas'] ;
            $atletasfc = json_decode(json_encode($atletas), True);
            $data['contadorAtletas'] = $atletasfc;

            $data['contadorComissao'] = $this->Futebol_model->contadorComissaoMenu();
            $atletas = $data['contadorComissao'] ;
            $atletasfc = json_decode(json_encode($atletas), True);
            $data['contadorComissao'] = $atletasfc;

            $data['contadorComissaoAntiga'] = $this->Futebol_model->contadorComissaoAntigaMenu();
            $atletas = $data['contadorComissaoAntiga'] ;
            $atletasfc = json_decode(json_encode($atletas), True);
            $data['contadorComissaoAntiga'] = $atletasfc;

            $data['contadorAtletasAntigas'] = $this->Futebol_model->contadorAtletasAntigasMenu();
            $atletas = $data['contadorAtletasAntigas'] ;
            $atletasfc = json_decode(json_encode($atletas), True);
            $data['contadorAtletasAntigas'] = $atletasfc;

            $data['meio']   =  'futebol/baixar';
            $this->load->view('layout/layout',$data);
    }

    public function gerarPlanilha() {

        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        }

        if(!Permissao($this->perfil)){

             redirect('permissao');      

        }

      $time             = $this->input->post('time');
      $tipo             = $this->input->post('tipo');
      $status           = $this->input->post('status');

      $this->load->library('PHPExcel');
      $this->load->helper('download');
      
      $arquivo = './assents/arquivos/planilhas/relatoriogeral.xlsx';
      $planilha = $this->phpexcel;

      $registros = $this->Futebol_model->listarExecel($time, $tipo, $status);
      $conversao = $registros ;
      $conversao2 = json_decode(json_encode($conversao), True);
      $registros = $conversao2;

      $planilha->setActiveSheetIndex(0)->setCellvalue('A1', 'NOME');
      $planilha->setActiveSheetIndex(0)->setCellvalue('B1', 'APELIDO');
      $planilha->setActiveSheetIndex(0)->setCellvalue('C1', 'CPF');
      $planilha->setActiveSheetIndex(0)->setCellvalue('D1', 'RG');
      $planilha->setActiveSheetIndex(0)->setCellvalue('E1', 'ENDERECO');
      $planilha->setActiveSheetIndex(0)->setCellvalue('F1', 'CELULAR');
      $planilha->setActiveSheetIndex(0)->setCellvalue('G1', 'DATA NASCIMENTO');
      $planilha->setActiveSheetIndex(0)->setCellvalue('H1', 'NOME MÃE');
      $planilha->setActiveSheetIndex(0)->setCellvalue('I1', 'NOME PAI');
      $planilha->setActiveSheetIndex(0)->setCellvalue('J1', 'POSOÇÃO');
      $contador = 1;

      foreach ($registros as $linha) {
         $contador++;
         $planilha->setActiveSheetIndex(0)->setCellvalue('A'.$contador, $linha['nome']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('B'.$contador, $linha['apelido']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('C'.$contador, $linha['cpf']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('D'.$contador, $linha['rg']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('E'.$contador, $linha['ende']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('F'.$contador, $linha['celular']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('G'.$contador, $linha['data_nascimento']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('H'.$contador, $linha['nomem']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('I'.$contador, $linha['nomep']);
         $planilha->setActiveSheetIndex(0)->setCellvalue('J'.$contador, $linha['posicao']);
      }

      $planilha->getActiveSheet()->setTitle('Relatorio');

      $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
      $objgravar->save($arquivo);


      force_download($arquivo, NULL);

      redirect('futebol/baixar'); 

  }

  function generate_qrcode($id)
    {
        /* Load QR Code Library */
        $this->load->library('Ciqrcode');
        $this->load->helper('download');
        
        /* Data */
        $hex_data   = bin2hex($id);
        $save_name  = $hex_data.'.png';

        /* QR Code File Directory Initialize */
        $dir = './assents/media/qrcode/';
        if (!file_exists($dir)) {
            mkdir($dir, 0775, true);
        }

        /* QR Configuration  */
        $config['cacheable']    = true;
        $config['imagedir']     = $dir;
        $config['quality']      = true;
        $config['size']         = '1024';
        $config['black']        = array(255,255,255);
        $config['white']        = array(255,255,255);
        $this->ciqrcode->initialize($config);

        $dados  = array(
                        'id' => $id
                  );
                  
        $resultado = $this->Futebol_model->editarAtleta($dados);
  
        /* QR Data  */
        $params['data']     = base_url()."assents/arquivos/documentos/".$resultado[0]->foto_doc;
        $params['level']    = 'L';
        $params['size']     = 10;
        $params['savename'] = FCPATH.$config['imagedir']. $save_name;
        
        $this->ciqrcode->generate($params);

        force_download($dir. $save_name, NULL);
        
    }


}
