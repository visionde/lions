<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Welcome');
		$this->load->model('Noticias_model');
		$this->load->library('session');
	}

	/**
	 * [index description]Redireciona para o login do sistema, este é o controller default e metodo padrão
	 * @return [type] [description]
	 */


    public function index()
	{
            $data['dados']  = $this->Noticias_model->consultaNoticias();
            $noticias = $data['dados'] ;
            $noticiasfc = json_decode(json_encode($noticias), True);
            $data['dados'] = $noticiasfc;

            $data['dadosprincipal']  = $this->Noticias_model->consultaNoticiasPrincipal();
            $noticiasP = $data['dadosprincipal'] ;
            $noticiasfcP = json_decode(json_encode($noticiasP), True);
            $data['dadosprincipal'] = $noticiasfcP;


			$data['meio']   = 'dashboard';
			$this->load->view('layout/layout',$data);

	}	

	public function noticias($id)
	{
            $data['dados']  = $this->Noticias_model->consultaNoticiasVisualizacao($id);
            $noticias = $data['dados'] ;
            $noticiasfc = json_decode(json_encode($noticias), True);
            $data['dados'] = $noticiasfc;

			$data['meio']   = 'noticias';
			$this->load->view('layout/layout',$data);

	}


	// /**
	//  * [processarLogin description] Método responsável para processar os dadoso de usuário e senha do sistema
	//  * @return [type] [description]
	//  */
	public function processarLogin()
	{
        $dados['usuario'] = $this->input->post('usuario');
        $dados['senha'] = sha1(md5(strtolower($this->input->post('senha'))));
        $host = $_SERVER["HTTP_HOST"];
        $host = explode('.',$host);


        $resultado = $this->Welcome->processarLogin($dados);


        if ($resultado) {


			$sessao = array(
				            'id'     => $resultado[0]->id,
							'usuario'   => $resultado[0]->usuario,
							'perfil_nome'    => $resultado[0]->perfil_nome,
							'perfil_id'      => $resultado[0]->perfil_id,
							);

	
			$this->session->set_userdata($sessao);

			$dados = $sessao['perfil_nome'];

			// var_dump($sessao['perfil_nome']); exit();


			// {Inclusão de modulo na sessão}
			$modulo = $this->Welcome->processarModulos();

			$perfil = $this->Welcome->perfil($dados);

			
	        $valores = array();	            

			foreach ($modulo as $dados) {				

		    	$valores[$dados->modulo] =  $dados->situacao;
			    
			}


		    $valoresP = array();	            

			foreach ($perfil as $dados) {				

		    	$valoresP[$dados->menu] =  $dados->situacao;
			    
			}	

			// var_dump($valoresP);die();		
		   
			$this->session->set_userdata('modulo',$valores); 

			$this->session->set_userdata('perfil',$valoresP); 
			

           if ($this->session->userdata('caminho'))
           	redirect($this->session->userdata('caminho'));

           redirect('site');


   //          $data['meio']   = 'dashboard';
			// $this->load->view('layout/layout',$data);
		
		}else{

			$this->session->set_flashdata('erro','Login ou senha inválidos!');	

			redirect('site');		
			
   //          $data['meio']   = 'dashboard';
			// $this->load->view('layout/layout',$data);

        }


		
	}

    /**
	 * [logout description]Ação de Logout do sistema
	 * @return [type] [description]
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('site');
	}



}
