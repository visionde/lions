<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissao extends CI_Controller {

   public function __construct()
	{   
		parent::__construct();
		$this->load->library('session');
	}


	public function index()
	{
		  $data['meio']   = 'layout/acesso';     
          $this->load->view('layout/layout',$data); 
	}

}