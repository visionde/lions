<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	private $modulo = "usuario";
	private $perfil = "USUARIO";

	public function __construct()
	{   
		parent::__construct();		
		$this->load->model('Usuarios_model');
		$this->load->library('session');
		$this->load->library('form_validation');

	}

	 public function index($page = 1, $pesquisa = NULL)
	{
	  if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

	  if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }


		$data['dadosContador'] = $this->Usuarios_model->contadorUsuarios();
	    $noticias = $data['dadosContador'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosContador'] = $noticiasfc;


        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 8;
        $data['dados']  = $this->Usuarios_model->listar($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'usuarios/usuarios';
        $total = $this->Usuarios_model->contador($page, $pesquisa);

        if ($total){
            $data['total_noticias']   = $total[0]->total;
        }else{
            $data['total_noticias']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
		$data['itens_per_page'] = $itens_per_page;
		$this->load->view('layout/layout',$data);

	}

	  public function cadastrarUsuario()
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }

		    $data['dadosContador'] = $this->Usuarios_model->contadorUsuarios();
		    $noticias = $data['dadosContador'] ;
	        $noticiasfc = json_decode(json_encode($noticias), True);
	        $data['dadosContador'] = $noticiasfc;

			$data['meio']   = 'usuarios/cadastrarUsuario';
			$this->load->view('layout/layout',$data);
		
	}
	

	public function cadastrarUsuarioExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }
		
		$this->form_validation->set_rules('nome', 'Nome', 'trim|required|is_unique[login.nome]');
		$this->form_validation->set_rules('usuario', 'Usuario', 'trim|required|is_unique[login.usuario]');
	    $this->form_validation->set_rules('senha', 'Senha', 'trim|required|differs[usuario]');
	    $this->form_validation->set_rules('senha2','Confirmação de senha', 'trim|required|matches[senha]');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->cadastrarUsuario();
        } else {  
		
		$nome          = $this->input->post('nome');
		$usuario       = $this->input->post('usuario');
		$senha         = sha1(md5(strtolower($this->input->post('senha'))));
		$acesso        = $this->input->post('acesso');
		
		
		
		$dados = array(
						'nome'                 => $nome,
						'usuario'              => $usuario,
						'senha'                => $senha
						
			);
		
            
		$resultado = $this->Usuarios_model->cadastrarUsuarioExe($dados);
		
		if($resultado){
			$this->session->set_flashdata('success','Usuário inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao inserir o Usuário!');
		}

		redirect('usuarios'); 
        }
       				
	} 


}