<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendario extends CI_Controller {

	private $modulo = "calendario";
	private $perfil = "CALENDARIO";

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Calendario_model');
		$this->load->library('session');
		$this->load->library('form_validation');	
	}

     public function index()
	{


            $data['dados']  = $this->Calendario_model->consulta();

            $test = $data['dados'] ;

            $teste = json_decode(json_encode($test), True);

            $data['dados'] = $teste;
            //var_dump($teste[0]); exit();


            $data['dadosRealizadas']  = $this->Calendario_model->consultaRR();

            $test = $data['dadosRealizadas'] ;

            $teste = json_decode(json_encode($test), True);

            $data['dadosRealizadas'] = $teste;

			$data['meio']   = 'calendario';
			$this->load->view('layout/layout',$data);

	}

	 public function calendarioadm()
	{

		// var_dump($this->perfil); exit();

		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

	     if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}



		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;


        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $data['times'] = $this->Calendario_model->times();


        $data['meio']   = 'calendario/calendarioadm';
	
	    $this->load->view('layout/layout',$data);

	}


  public function cadastrarPartidasExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		 if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}
		
        $this->form_validation->set_rules('torneio','Torneio','trim|required');
        $this->form_validation->set_rules('local','Local da Partida','trim|required');
        $this->form_validation->set_rules('data','Data da Partida','trim|required');
        $this->form_validation->set_rules('horario','Horário da Partida','trim|required');
        $this->form_validation->set_rules('casa','Time Casa','trim|required');
        $this->form_validation->set_rules('visitante','Time Visitante','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->calendarioadm();
        } else {  
		
		$torneio          = $this->input->post('torneio');
		$local            = $this->input->post('local');
		$data             = $this->input->post('data');
		$horario          = $this->input->post('horario');
		$casa             = $this->input->post('casa');
		$visitante        = $this->input->post('visitante');
	
		
		$dados = array(
						'torneio'                        => $torneio,
						'local_partida'                  => $local,
						'data_partida'                   => date("Y-m-d", strtotime($data)),
						'hora_partida'                   => $horario,
						'id_time_casa'                   => $casa,
						'id_time_visitante'              => $visitante
						
			);
		// var_dump($dados); exit();
		
            
		$resultado = $this->Calendario_model->cadastrarPartidasExe($dados);
		
		if($resultado){
			$this->session->set_flashdata('success','Partida inserida com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao inserir a Partida!');
		}

		redirect('calendario/calendarioadm'); 
        }
       		

	}

   public function editarProximas($id)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}
		
		$dados  = array(
				'id' => $id
		    	);
		    	

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;

        $data['times'] = $this->Calendario_model->times();
            	
		$resultado = $this->Calendario_model->editarProximas($dados);    	  	
		
			
		$data['meio']   =  'calendario/editarProximas';
		$data['dados']  =  $resultado[0];
		$this->load->view('layout/layout',$data);
     
	}

	 public function editarProximasExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}


	    if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}
		
        $this->form_validation->set_rules('torneio','Torneio','trim|required');
        $this->form_validation->set_rules('local','Local da Partida','trim|required');
        $this->form_validation->set_rules('data','Data da Partida','trim|required');
        $this->form_validation->set_rules('horario','Horário da Partida','trim|required');
        $this->form_validation->set_rules('casa','Time Casa','trim|required');
        $this->form_validation->set_rules('visitante','Time Visitante','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        $id = $this->input->post('id');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editarProximas($id);
        } else {  
		
		$torneio          = $this->input->post('torneio');
		$local            = $this->input->post('local');
		$data             = $this->input->post('data');
		$horario          = $this->input->post('horario');
		$casa             = $this->input->post('casa');
		$visitante        = $this->input->post('visitante');
	
		
		$dados = array(
						'torneio'                        => $torneio,
						'local_partida'                  => $local,
						'data_partida'                   => date("Y-m-d", strtotime($data)),
						'hora_partida'                   => $horario,
						'id_time_casa'                   => $casa,
						'id_time_visitante'              => $visitante
						
			);
		// var_dump($dados); exit();
		
            
		$resultado = $this->Calendario_model->editarProximasExe($dados,$id);
		
		if($resultado){
			$this->session->set_flashdata('success','Partida editada com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao editada a Partida!');
		}

		redirect('calendario/proximasPartidas'); 
        }
       		

	}

	 public function excluirProximas($id)
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}	


	    if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}    

		$dados  = array(
				   'id' => $id
					  );
		$resultado = $this->Calendario_model->excluirProximas($dados);

		if($resultado){
			$this->session->set_flashdata('success','Partida excluida com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir a Partida!');
		}

		redirect('calendario/proximasPartidas','refresh');		

	}	

   public function editarRealizadas($id)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}


	    if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}
		
		$dados  = array(
				'id' => $id
		    	);
		    	

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $data['times'] = $this->Calendario_model->times();
            	
		$resultado = $this->Calendario_model->editarRealizadas($dados);    	  	
		
			
		$data['meio']   =  'calendario/editarRealizadas';
		$data['dados']  =  $resultado[0];
		$this->load->view('layout/layout',$data);
     
	}

  public function editarRealizadasExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}


	    if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}
		
        $this->form_validation->set_rules('torneio','Torneio','trim|required');
        $this->form_validation->set_rules('local','Local da Partida','trim|required');
        $this->form_validation->set_rules('data','Data da Partida','trim|required');
        $this->form_validation->set_rules('horario','Horário da Partida','trim|required');
        $this->form_validation->set_rules('casa','Time Casa','trim|required');
        $this->form_validation->set_rules('visitante','Time Visitante','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        $id = $this->input->post('id');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editarRealizadas($id);
        } else {  
		
		$torneio          = $this->input->post('torneio');
		$local            = $this->input->post('local');
		$data             = $this->input->post('data');
		$horario          = $this->input->post('horario');
		$casa             = $this->input->post('casa');
		$visitante        = $this->input->post('visitante');
	
		
		$dados = array(
						'torneio'                        => $torneio,
						'local_partida'                  => $local,
						'data_partida'                   => date("Y-m-d", strtotime($data)),
						'hora_partida'                   => $horario,
						'id_time_casa'                   => $casa,
						'id_time_visitante'              => $visitante
						
			);
		// var_dump($dados); exit();
		
            
		$resultado = $this->Calendario_model->editarRealizadasExe($dados,$id);
		
		if($resultado){
			$this->session->set_flashdata('success','Partida editada com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao editada a Partida!');
		}

		redirect('calendario/partidasRealizadas'); 
        }
       		

	}

  public function excluirRealizadas($id)
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	    

		$dados  = array(
				   'id' => $id
					  );
		$resultado = $this->Calendario_model->excluirRealizadas($dados);

		if($resultado){
			$this->session->set_flashdata('success','Partida excluida com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir a Partida!');
		}

		redirect('calendario/partidasRealizadas','refresh');		

	}


  public function editarResultados($id)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}
		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		
		$dados  = array(
				'id' => $id
		    	);
		    	

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $data['times'] = $this->Calendario_model->times();
            	
		$resultado = $this->Calendario_model->editarResultados($dados);    	  	
		
			
		$data['meio']   =  'calendario/editarResultados';
		$data['dados']  =  $resultado[0];
		$this->load->view('layout/layout',$data);
     
	}


		 public function editarResultadosExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		

        $this->form_validation->set_rules('gol_time_casa','Gols Time Casa','trim|required');
        $this->form_validation->set_rules('gol_time_visitante','Gols Time Visitante','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        $id = $this->input->post('id');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editarResultados($id);
        } else {  
		

		$gol_time_casa                = $this->input->post('gol_time_casa');
		$gol_time_visitante           = $this->input->post('gol_time_visitante');
		$penalti_time_visitante       = $this->input->post('penalti_time_casa');
		$penalti_time_casa            = $this->input->post('penalti_time_visitante');
	
		
		$dados = array(
						'gol_time_casa    '                        => $gol_time_casa,
						'gol_time_visitante'                       => $gol_time_visitante,
						'penalti_time_visitante'                   => $penalti_time_visitante,
						'penalti_time_casa'                        => $penalti_time_casa
						
			);

		// var_dump($dados); exit();
            
		$resultado = $this->Calendario_model->editarResultadosExe($dados,$id);
		
		if($resultado){
			$this->session->set_flashdata('success','Resultado editado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao editar o Resultado!');
		}

		redirect('calendario/resultados'); 
        }
       		

	}


	  public function adicionarResultados($id)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		
		$dados  = array(
				'id' => $id
		    	);
		    	

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $data['times'] = $this->Calendario_model->times();
            	
		$resultado = $this->Calendario_model->editarResultados($dados);    	  	
		
			
		$data['meio']   =  'calendario/adicionarResultados';
		$data['dados']  =  $resultado[0];
		$this->load->view('layout/layout',$data);
     
	}

		 public function adicionarResultadosExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		

        $this->form_validation->set_rules('gol_time_casa','Gols Time Casa','trim|required');
        $this->form_validation->set_rules('gol_time_visitante','Gols Time Visitante','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        $id = $this->input->post('id');


        if ($this->form_validation->run() == FALSE)
        {
                $this->adicionarResultados($id);
        } else {  
		

		$gol_time_casa                = $this->input->post('gol_time_casa');
		$gol_time_visitante           = $this->input->post('gol_time_visitante');
		$penalti_time_visitante       = $this->input->post('penalti_time_casa');
		$penalti_time_casa            = $this->input->post('penalti_time_visitante');
	
		
		$dados = array(
						'gol_time_casa    '                        => $gol_time_casa,
						'gol_time_visitante'                       => $gol_time_visitante,
						'penalti_time_visitante'                   => $penalti_time_visitante,
						'penalti_time_casa'                        => $penalti_time_casa
						
			);
            
		$resultado = $this->Calendario_model->editarResultadosExe($dados,$id);
		
		if($resultado){
			$this->session->set_flashdata('success','Resultado adicionado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao adicionar o Resultado!');
		}

		redirect('calendario/resultados'); 
        }
       		

	}


	  public function times($page = 1, $pesquisa = NULL)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');

		 }  

		 if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	 

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 5;
        $data['dados']  = $this->Calendario_model->listarTimes($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'calendario/times';
        $total = $this->Calendario_model->contadorTimes($page, $pesquisa);

        if ($total){
            $data['total']   = $total[0]->total;
        }else{
            $data['total']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
		$data['itens_per_page'] = $itens_per_page;

			
	    $this->load->view('layout/layout',$data);

		
    }


  public function editarTimes($id)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		
		$dados  = array(
				'id' => $id
		    	);
		    	

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;



        $data['times'] = $this->Calendario_model->times();
            	
		$resultado = $this->Calendario_model->editarTimes($dados);    	  	
		
			
		$data['meio']   =  'calendario/editarTimes';
		$data['dados']  =  $resultado[0];
		$this->load->view('layout/layout',$data);
     
	}

 public function editarTimesExe()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		

        $this->form_validation->set_rules('time','Nome do Time','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        $id = $this->input->post('id');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editarTimes($id); 
        } else {  
		
		$time            = $this->input->post('time');
	
		
		$dados = array(

						'time'                        => $time
						
			);

		// var_dump($dados); exit();
            
		$resultado = $this->Calendario_model->editarTimesExe($dados,$id);
		
		if($resultado){
			$this->session->set_flashdata('success','Time editado com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao editar o Time!');
		}

		redirect('calendario/times'); 
        }
       		

	}

	  public function adicionarTimes()
	{
		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	


		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;


        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $data['times'] = $this->Calendario_model->times();


        $data['meio']   = 'calendario/adicionarTimes';
	
	    $this->load->view('layout/layout',$data);
     
	}
  
  public function adicionarTimesExe()
	{
		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	

        $this->form_validation->set_rules('time','Nome do Time','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->adicionarTimes();
        } else {  
		
		$time        = $this->input->post('time');
	
		
		$dados = array(

						'time'              => $time
						
			);
		// var_dump($dados); exit();
		
            
		$resultado = $this->Calendario_model->adicionarTimesExe($dados);
		
		if($resultado){
			$this->session->set_flashdata('success','Time inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao inserir o Time!');
		}

		redirect('calendario/times'); 
        }

}

	 public function proximasPartidas($page = 1, $pesquisa = NULL)
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	


		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 5;
        $data['dados']  = $this->Calendario_model->listarProximas($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'calendario/proximasPartidas';
        $total = $this->Calendario_model->contadorProximas($page, $pesquisa);

        if ($total){
            $data['total']   = $total[0]->total;
        }else{
            $data['total']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
		$data['itens_per_page'] = $itens_per_page;

			
	    $this->load->view('layout/layout',$data);

	}

   public function partidasRealizadas($page = 1, $pesquisa = NULL)
	{
		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}


		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 5;
        $data['dados']  = $this->Calendario_model->listarRealizadas($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'calendario/partidasRealizadas';
        $total = $this->Calendario_model->contadorRealizadas($page, $pesquisa);

        if ($total){
            $data['total']   = $total[0]->total;
        }else{
            $data['total']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
		$data['itens_per_page'] = $itens_per_page;

			
	    $this->load->view('layout/layout',$data);

	}


	   public function resultados($page = 1, $pesquisa = NULL)
	{

		 if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

		    redirect('permissao');		  

		}	
		

		$data['dadosProximas'] = $this->Calendario_model->contadorProximasMenu();
	    $noticias = $data['dadosProximas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosProximas'] = $noticiasfc;

        $data['dadosRealizadas'] = $this->Calendario_model->contadorRealizadasMenu();
	    $noticias = $data['dadosRealizadas'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosRealizadas'] = $noticiasfc;

        $data['dadosResultado'] = $this->Calendario_model->contadorResultadosMenu();
	    $noticias = $data['dadosResultado'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosResultado'] = $noticiasfc;

        $data['dadosTime'] = $this->Calendario_model->contadorTimesMenu();
	    $noticias = $data['dadosTime'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosTime'] = $noticiasfc;


        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 5;
        $data['dados']  = $this->Calendario_model->resultados($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'calendario/resultados';
        $total = $this->Calendario_model->contadorResultados($page, $pesquisa);

        if ($total){
            $data['total']   = $total[0]->total;
        }else{
            $data['total']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
		$data['itens_per_page'] = $itens_per_page;

			
	    $this->load->view('layout/layout',$data);

	}


}
