<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fisioterapeuta extends CI_Controller {

      private $modulo = "fisioterapeuta";
      private $perfil = "FISIOTERAPEUTA";

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Fisioterapeuta_model');
		$this->load->library('session');
	}

    public function index()
	{
	  if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

	  if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }

        $data['dados']  = $this->Fisioterapeuta_model->listar();
		$data['meio']   = 'fisioterapeuta/fisioterapeuta';
		$this->load->view('layout/layout',$data);
	}

	public function relacaoExe()
     {

            if(!verificarPermissao($this->modulo)){

                      redirect('permissao');            

                  }
            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }
            
        $this->form_validation->set_rules('mes','Mes','trim|required');
        $this->form_validation->set_rules('ano','ANO','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() == FALSE){
	         
	         $this->index();

	    }else {  


	    $mes             = $this->input->post('mes');
		$ano             = $this->input->post('ano');
	
				

	     $resultado = $this->Fisioterapeuta_model->verificacao($mes, $ano);

	    if($resultado){
			$this->session->set_flashdata('erro','Erro ao inserir Planilha a mesma já existe com esse Mês e Ano!');
		}else{
				    $array = array($_POST);

			  		$jogadoras = array();

			  		$arrTamanho = (sizeof($array[0]) - 2) / 3;

			  
					for($i=0; $i < $arrTamanho; $i++){

						$lesao         = 'lesao'.$i;
						$treinamento   = 'treinamento'.$i;
						$nome          = 'nome'.$i;
						$mes           = 'mes';
						$ano           = 'ano';

						$jogadora['lesao']       = $array[0][$lesao];
						$jogadora['treinamento'] = $array[0][$treinamento];
						$jogadora['nome']        = $array[0][$nome];
						$jogadora['mes']         = $array[0][$mes];
						$jogadora['ano']         = $array[0][$ano];


						$jogadoras[$i] = $jogadora;
					 }


				     $resultado = $this->Fisioterapeuta_model->relacaoExe($jogadoras);

				     if($resultado){
						$this->session->set_flashdata('success','Planilha inserida com sucesso!');
					}else{
						$this->session->set_flashdata('erro','Erro ao inserir a Planilha!');
					}
		
		}

		redirect('fisioterapeuta'); 


	   }
    }

   public function editar()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }


            $data['meio']   =  'fisioterapeuta/editar';
            $this->load->view('layout/layout',$data);
  }

   public function editarExe()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

		    $mes             = $this->input->post('mes');
			$ano             = $this->input->post('ano');


	        $data['dados'] = $this->Fisioterapeuta_model->editarExe($mes, $ano);
 
            $data['meio']   =  'fisioterapeuta/editarPlanilha';
            $this->load->view('layout/layout',$data);
  }

   public function editarPlanilha()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }

            $array = array($_POST);

            $jogadoras = array();

	  		$arrTamanho = (sizeof($array[0])) / 4;


	  
			for($i=0; $i < $arrTamanho; $i++){

				$lesao         = 'lesao'.$i;
				$treinamento   = 'treinamento'.$i;
				$nome          = 'nome'.$i;
				$id            = 'id'.$i;

				$jogadora['lesao']       = $array[0][$lesao];
				$jogadora['treinamento'] = $array[0][$treinamento];
				$jogadora['nome']        = $array[0][$nome];
				$jogadora['id']          = $array[0][$id];


				$jogadoras[$i] = $jogadora;
			 }


     	   $resultado = $this->Fisioterapeuta_model->editarPlanilha($jogadoras);

		     if($resultado){
				$this->session->set_flashdata('success','Planilha inserida com sucesso!');
			}else{
				$this->session->set_flashdata('erro','Erro ao inserir a Planilha!');
			}

       redirect('fisioterapeuta/editar'); 

  }

  public function baixar()
      {
            if(!verificarPermissao($this->modulo)){

                redirect('permissao');            

            }

            if(!Permissao($this->perfil)){

                 redirect('permissao');      

            }


            $data['meio']   =  'fisioterapeuta/baixar';
            $this->load->view('layout/layout',$data);
  }


 public function gerarPlanilha()
  {
        if(!verificarPermissao($this->modulo)){

            redirect('permissao');            

        }

        if(!Permissao($this->perfil)){

             redirect('permissao');      

        }

	    $mes             = $this->input->post('mes');
		$ano             = $this->input->post('ano');


	    


      $this->load->library('PHPExcel');
      $this->load->helper('download');
      
      $arquivo = './assents/arquivos/planilhas/relatorio.xlsx';
      $planilha = $this->phpexcel;

      $registros = $this->Fisioterapeuta_model->editarExe($mes, $ano);
      $conversao = $registros ;
	  $conversao2 = json_decode(json_encode($conversao), True);
	  $registros = $conversao2;

      $planilha->setActiveSheetIndex(0)->setCellvalue('A1', 'NOME');
      $planilha->setActiveSheetIndex(0)->setCellvalue('B1', 'LESAO');
      $planilha->setActiveSheetIndex(0)->setCellvalue('C1', 'TREINAMENTO');
      $contador = 1;

      foreach ($registros as $linha) {
      	 $contador++;
      	 $planilha->setActiveSheetIndex(0)->setCellvalue('A'.$contador, $linha['nome']);
      	 $planilha->setActiveSheetIndex(0)->setCellvalue('B'.$contador, $linha['lesao']);
      	 $planilha->setActiveSheetIndex(0)->setCellvalue('C'.$contador, $linha['treinamento']);
      }

      $planilha->getActiveSheet()->setTitle('Planilha 1');

      $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
      $objgravar->save($arquivo);


      force_download($arquivo, NULL);

      redirect('fisioterapeuta/baixar'); 

  }



}