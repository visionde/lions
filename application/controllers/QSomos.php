<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QSomos extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Welcome');
		$this->load->library('session');
	}

     public function index()
	{

			$data['meio']   = 'quem_somos';
			$this->load->view('layout/layout',$data);

	}


}
