<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller {

	private $modulo = "noticias";
	private $perfil = "NOTICIAS";

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Noticias_model');
		$this->load->library('session');
		$this->load->library('form_validation');
	}

   public function index()
	{
	  if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }
		    $data['dados'] = $this->Noticias_model->contadorNoticias();
		    $noticias = $data['dados'] ;
            $noticiasfc = json_decode(json_encode($noticias), True);
            $data['dados'] = $noticiasfc;

		    // var_dump($data['dados']); exit();

			$data['meio']   = 'noticias/noticiasadm';
			$this->load->view('layout/layout',$data);

	}

   public function PublicarNoticias()
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }

		
		$this->form_validation->set_rules('titulo','Título','trim|required');
		if (empty($_FILES['imagem_destaque']['name']))
			{
			    $this->form_validation->set_rules('imagem_destaque', 'Imagem Destaque', 'required');
			}

		if (empty($_FILES['imagem_mini']['name']))
			{
			    $this->form_validation->set_rules('imagem_mini', 'Imagem Miniatura', 'required');
			}
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->index();
        } else {  

        $arrDadosArquivo["type"]      = $_FILES["imagem_destaque"]["type"];
        $arrDadosArquivo2["type"]     = $_FILES["imagem_mini"]["type"];
        $arquivo                      = $_FILES['imagem_destaque']['tmp_name'];
        $arquivo2                     = $_FILES['imagem_mini']['tmp_name'];

        $separador = explode('/', $arrDadosArquivo["type"]);
        $separador = $separador[1];

		        if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
		           
		             $this->session->set_flashdata('erro','Extenção Imagem Destaque invalida. Tente Novamente!');
				     $this->index();
		         }
		$separador2 = explode('/', $arrDadosArquivo2["type"]);
        $separador2 = $separador2[1];
        		if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
		           
		             $this->session->set_flashdata('erro','Extenção Imagem Miniatura invalida. Tente Novamente!');
				     $this->index();
		         }

		$imagem_destaque = date('dmYhis') . '.' . 'jpeg';
		$imagem_mini     = date('dmYhis') . '.' . 'jpeg';
		
	     $configuracao = array(
	         'upload_path'   => './assents/arquivos/noticias/destaque',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
	         'file_name'     => $imagem_destaque,
	         'max_size'      => '0'
	     );      
	     $this->load->library('upload');
	     $this->upload->initialize($configuracao);
	     if ($this->upload->do_upload('imagem_destaque'))
	         $destaque = true;
	     else
	         echo $this->upload->display_errors();

	     $configuracao2 = array(
	         'upload_path'   => './assents/arquivos/noticias/mini',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
	         'file_name'     => $imagem_mini,
	         'max_size'      => '0'
	     ); 

$mini =  true;
 $destaque = true;

	     $this->load->library('upload');
	     $this->upload->initialize($configuracao2);
	     if ($this->upload->do_upload('imagem_mini'))
	         $mini =  true;
	     else
	         echo $this->upload->display_errors();


        if($destaque == true and $mini == true) // se o arquivo foi despejado corretamente
          {
				$titulo          = $this->input->post('titulo');
				$corpo           = $this->input->post('corpo');
				$miniIMG            = $imagem_mini;
				$destaqueIMG        = $imagem_destaque;
				
				
				
				$dados = array(
								'titulo'                 => $titulo ,
								'corpo'                  => $corpo,
								'destaque_imagem'        => $destaqueIMG,
								'mini_imagem'            => $miniIMG
								
					);

				// var_dump($dados); exit();

				$resultado = $this->Noticias_model->cadastrarNoticiasExe($dados);
				
				if($resultado){
					$this->session->set_flashdata('success','Noticias publicada com sucesso!');
				}else{
					$this->session->set_flashdata('erro','Erro ao inserir Noticias!');
				}

				redirect('noticias'); 
		   }

    //     $this->load->library('ftp');

    //     $config['hostname'] = API_NOTICIAS_HOST;
    //     $config['username'] = API_NOTICIAS_USER;
    //     $config['password'] = API_NOTICIAS_PASS;
    //     $config['port']     = 21;
    //     $config['passive']  = TRUE;
      

    //     $this->ftp->connect($config);


    //     $resultado = $this->ftp->upload($arquivo,'/public_html/lionsfc.com.br/assents/arquivos/noticias/destaque/'.$imagem_destaque, 'auto', 0775);

    //     $resultado2 = $this->ftp->upload($arquivo2,'/public_html/lionsfc.com.br/assents/arquivos/noticias/mini/'.$imagem_mini, 'auto', 0775);
    //     $this->ftp->close();


    //      if($resultado == true and $resultado2 == true) // se o arquivo foi despejado corretamente
    //       {
				// $titulo          = $this->input->post('titulo');
				// $corpo           = $this->input->post('corpo');
				// $mini            = $imagem_mini;
				// $destaque        = $imagem_destaque;
				
				
				
				// $dados = array(
				// 				'titulo'                 => $titulo ,
				// 				'corpo'                  => $corpo,
				// 				'destaque_imagem'        => $destaque,
				// 				'mini_imagem'            => $mini
								
				// 	);

				// // var_dump($dados); exit();

				// $resultado = $this->Noticias_model->cadastrarNoticiasExe($dados);
				
				// if($resultado){
				// 	$this->session->set_flashdata('success','Noticias publicada com sucesso!');
				// }else{
				// 	$this->session->set_flashdata('erro','Erro ao inserir Noticias!');
				// }

				// redirect('noticias'); 
		  //       }
           }

       				
	}


  public function Listar($page = 1, $pesquisa = NULL)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }

		$data['dadosContador'] = $this->Noticias_model->contadorNoticias();
	    $noticias = $data['dadosContador'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosContador'] = $noticiasfc;


        $pesquisa = isset($_GET['pesquisa']) ? $_GET['pesquisa'] : $pesquisa;
        $itens_per_page = 8;
        $data['dados']  = $this->Noticias_model->listar($page, $itens_per_page, $pesquisa);
        $data['meio']   = 'noticias/listar';
        $total = $this->Noticias_model->contador($page, $pesquisa);

        if ($total){
            $data['total_noticias']   = $total[0]->total;
        }else{
            $data['total_noticias']   = null;
        }
        $data['page']   = $page;
        $data['pesquisa']   = $pesquisa;
		$data['itens_per_page'] = $itens_per_page;
		$this->load->view('layout/layout',$data);

	}  


 public function editar($id)
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }
		
		$dados  = array(
				'id' => $id
		    	);
		    	
		$resultado = $this->Noticias_model->editar($dados);  

	    $data['dadosContador'] = $this->Noticias_model->contadorNoticias();
	    $noticias = $data['dadosContador'] ;
        $noticiasfc = json_decode(json_encode($noticias), True);
        $data['dadosContador'] = $noticiasfc;
  	  	
			
		$data['meio']   =  'noticias/editar';
		$data['dados']  =  $resultado[0];
		$this->load->view('layout/layout',$data);
     
	}

public function editarExe()
	{
  		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}

		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }
		$id = $this->input->post('id');
		
		$this->form_validation->set_rules('titulo','Título','trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


        if ($this->form_validation->run() == FALSE)
        {
                $this->editar($id);
        }else{


        $arrDadosArquivo["type"]      = $_FILES["imagem_destaque"]["type"];
        $arrDadosArquivo2["type"]     = $_FILES["imagem_mini"]["type"];
        $arquivo                      = $_FILES['imagem_destaque']['tmp_name'];
        $arquivo2                     = $_FILES['imagem_mini']['tmp_name'];


        // var_dump($arquivo. '--'.$arquivo2); exit();

        if ($arquivo != NULL and $arquivo2 == NULL) {

        $separador = explode('/', $arrDadosArquivo["type"]);
        $separador = $separador[1];

		        if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
		           
		             $this->session->set_flashdata('erro','Extenção Imagem Destaque invalida. Tente Novamente!');
				     $this->index();
		         }


		$imagem_destaque = date('dmYhis') . '.' . 'jpeg';

	     $configuracao = array(
	         'upload_path'   => './assents/arquivos/noticias/destaque',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
	         'file_name'     => $imagem_destaque,
	         'max_size'      => '0'
	     );      
	     $this->load->library('upload');
	     $this->upload->initialize($configuracao);
	     if ($this->upload->do_upload('imagem_destaque'))
	         $destaque = true;

	        if($destaque == true) // se o arquivo foi despejado corretamente
	          {
					$titulo          = $this->input->post('titulo');
					$corpo           = $this->input->post('corpo');
					$destaqueIMG        = $imagem_destaque;
					
					
					
					$dados = array(
									'titulo'                 => $titulo ,
									'corpo'                  => $corpo,
									'destaque_imagem'        => $destaqueIMG
									
						);

					// var_dump($dados); exit();

					$resultado = $this->Noticias_model->editarExe($dados,$id);
					
					if($resultado){
						$this->session->set_flashdata('success','Noticias publicada com sucesso!');
					}else{
						$this->session->set_flashdata('erro','Erro ao inserir Noticias!');
					}

					redirect('noticias/listar'); 
			   }

       }elseif ($arquivo == NULL and $arquivo2 != NULL)  {

		$separador2 = explode('/', $arrDadosArquivo2["type"]);
        $separador2 = $separador2[1];
        		if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
		           
		             $this->session->set_flashdata('erro','Extenção Imagem Destaque invalida. Tente Novamente!');
				     $this->index();
		         }


		$imagem_mini     = date('dmYhis') . '.' . 'jpeg';


        $configuracao2 = array(
	         'upload_path'   => './assents/arquivos/noticias/mini',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
	         'file_name'     => $imagem_mini,
	         'max_size'      => '0'
	     );  


	     $this->load->library('upload');
	     $this->upload->initialize($configuracao2);
	     if ($this->upload->do_upload('imagem_mini'))
	         $mini =  true;


        if($mini == true) // se o arquivo foi despejado corretamente
          {
				$titulo          = $this->input->post('titulo');
				$corpo           = $this->input->post('corpo');
				$miniIMG            = $imagem_mini;
			
				
				$dados = array(
								'titulo'                 => $titulo ,
								'corpo'                  => $corpo,
								'mini_imagem'            => $miniIMG
								
					);

				// var_dump($dados); exit();

				$resultado = $this->Noticias_model->editarExe($dados,$id);
				
				if($resultado){
					$this->session->set_flashdata('success','Noticias publicada com sucesso!');
				}else{
					$this->session->set_flashdata('erro','Erro ao inserir Noticias!');
				}

				redirect('noticias/listar');  
		   }
       }elseif($arquivo2 != NULL and $arquivo != NULL){


       	        $separador = explode('/', $arrDadosArquivo["type"]);
        $separador = $separador[1];

		        if(($separador <> 'png') and ($separador <> 'PNG') and ($separador <> 'jpg') and ($separador <> 'JPG') and ($separador <> 'JPEG') and ($separador <> 'jpeg')) {
		           
		             $this->session->set_flashdata('erro','Extenção Imagem Destaque invalida. Tente Novamente!');
				     $this->index();
		         }
		$separador2 = explode('/', $arrDadosArquivo2["type"]);
        $separador2 = $separador2[1];
        		if(($separador2 <> 'png') and ($separador2 <> 'PNG') and ($separador2 <> 'jpg') and ($separador2 <> 'JPG') and ($separador2 <> 'JPEG') and ($separador2 <> 'jpeg')) {
		           
		             $this->session->set_flashdata('erro','Extenção Imagem Destaque invalida. Tente Novamente!');
				     $this->index();
		         }

		$imagem_destaque = date('dmYhis') . '.' . 'jpeg';
		$imagem_mini     = date('dmYhis') . '.' . 'jpeg';
		
	     $configuracao = array(
	         'upload_path'   => './assents/arquivos/noticias/destaque',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
	         'file_name'     => $imagem_destaque,
	         'max_size'      => '500'
	     );      
	     $this->load->library('upload');
	     $this->upload->initialize($configuracao);
	     if ($this->upload->do_upload('imagem_destaque'))
	         $destaque = true;
	     else
	         echo $this->upload->display_errors();

	     $configuracao2 = array(
	         'upload_path'   => './assents/arquivos/noticias/mini',
	         'allowed_types' => 'jpeg|JPEG|jpg|JPG|png|PNG',
	         'file_name'     => $imagem_mini,
	         'max_size'      => '500'
	     );  


	     $this->load->library('upload');
	     $this->upload->initialize($configuracao2);
	     if ($this->upload->do_upload('imagem_mini'))
	         $mini =  true;
	     else
	         echo $this->upload->display_errors();



        if($destaque == true and $mini == true) // se o arquivo foi despejado corretamente
          {
				$titulo          = $this->input->post('titulo');
				$corpo           = $this->input->post('corpo');
				$miniIMG            = $imagem_mini;
				$destaqueIMG        = $imagem_destaque;
				
				
				
				$dados = array(
								'titulo'                 => $titulo ,
								'corpo'                  => $corpo,
								'destaque_imagem'        => $destaqueIMG,
								'mini_imagem'            => $miniIMG
								
					);

				// var_dump($dados); exit();

				$resultado = $this->Noticias_model->editarExe($dados,$id);
				
				if($resultado){
					$this->session->set_flashdata('success','Noticias publicada com sucesso!');
				}else{
					$this->session->set_flashdata('erro','Erro ao inserir Noticias!');
				}

				redirect('noticias/listar'); 
		   }


       }else{

       	        $titulo          = $this->input->post('titulo');
				$corpo           = $this->input->post('corpo');

				
				
				
				$dados = array(
								'titulo'                 => $titulo ,
								'corpo'                  => $corpo
								
					);

				// var_dump($dados); exit();

				$resultado = $this->Noticias_model->editarExe($dados,$id);
				
				if($resultado){
					$this->session->set_flashdata('success','Noticias editada com sucesso!');
				}else{
					$this->session->set_flashdata('erro','Erro ao editar Noticias!');
				}

				redirect('noticias/listar'); 
		        }


       }


}



  public function excluir($id)
	{
 		if(!verificarPermissao($this->modulo)){

		    redirect('permissao');		  

		}	


		if(!Permissao($this->perfil)){

                 redirect('permissao');      

       }    

		$id = $this->uri->segment(3);

		$dados  = array(
						'visivel' => 0
					  );

		$resultado = $this->Noticias_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','Notícia excluida com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir!');
		}

		redirect('noticias/listar','refresh');		

	}


}
