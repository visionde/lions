<?php
defined('BASEPATH') OR exit('No direct script access allowed');


function verificarPermissao($param)
{

    if($param == 'tabelas')
        return true;
          $CI = & get_instance();  //get instance, access the CI superobject
          $permissao = $CI->session->userdata('modulo')[$param];
         
          // var_dump($permissao); exit();

           if ($permissao){          

              return true;

           } else {                             

              return false;

           }


}


function Permissao($param)
{

    if($param == 'tabelas')
        return true;
          $CI = & get_instance();  //get instance, access the CI superobject
          $permissao = $CI->session->userdata('perfil')[$param];
         
           if ($permissao){          

              return true;

           } else {                             

              return false;

           }


}

function gerarPDF($html = '') {

    // Require composer autoload
    require_once __DIR__ . '/../../vendor/autoload.php';

    $mpdf = new mPDF('','A4', 12, '', 8, 8, 10, 10, 9, 9, 'L');

   // $stylesheet = file_get_contents('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    $stylesheet2 = "
      strong{
        font-weight: bold;
      }
      table{
      width: 100%;
      text-align:center;
      border: 4px solid #fff;
      border-collapse: collapse;
      border-top-color: #800000;
      color: white;
      }
      #DivA{
      float:left; /*<---*/
      width: 60%;
      }

      #DivB{
      float:left; /*<---*/
      width: 40%;
      }

      ";

    $mpdf->SetFooter(array (
      'odd' => array (
      'L' => array ('content' => '', 'font-size' => 8, 'font-style' => 'BI', 'font-family' => 'serif', 'color'=>'#000000'),
      'C' => array ('content' => 'LIONS FUTEBOL CLUBE - RECIFE - PERNAMBUCO', 'font-size' => 8, 'font-style' => 'BI', 'font-family' => 'cambria', 'color'=>'#000000'),
      'R' => array ('content' => '', 'font-size' => 8, 'font-style' => 'BI', 'font-family' => 'serif', 'color'=>'#000000'),
      'line' => 0,
          ),
      ));


   // $mpdf->WriteHTML($stylesheet,1); 
    $mpdf->WriteHTML($stylesheet2,1); 
    $mpdf->WriteHTML($html,2);
    ob_clean();  // eh  aqui que a mágica acontece!  :)
    $mpdf->Output('contrato.pdf', 'D');
    exit;
}