 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
  
 <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span8">
            <div class="widget">
              <h5 class="widgetheading">Criar Notícia</h5>
              <form id="commentform" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/PublicarNoticias" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span8">
                    <label class="form-group" for="descricao" >Título</label>
                    <input type="text" name="titulo" value="<?php echo set_value('titulo');?>"/>
                    <?php echo form_error('titulo');?><br>
                  </div>
                  <div class="span4">
                    <label class="form-group" for="descricao">Imagem Destaque</label>
                     <input type="file" id="imagem"   name="imagem_destaque">
                     <?php echo form_error('imagem_destaque');?><br>
                  </div>
                  <div class="span4">
                    <label class="form-group" for="descricao">Imagem Miniatura</label>
                     <input type="file" id="imagem"   name="imagem_mini">
                     <?php echo form_error('imagem_mini');?><br>
                  </div>
                  <div class="span8 margintop10">
                    <label class="form-group" for="descricao">Corpo Notícia</label>
                    <p>
                      <textarea id="editor1" rows="12" name="corpo" class="input-block-level" ></textarea>
                    </p>
                    <p>
                      <button class="btn btn-green" type="submit">Enviar</button>
                      <a href="<?php echo base_url(); ?>site" class="btn btn-red">Cancelar</a>
                    </p>
                  </div>  
                </div>
              </form>
            </div>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Notícias</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>noticias">Criar Notícia</a></li>
                  <?php foreach ($dados as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/listar">Listar Notícias</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      window.onload = function()  {
        CKEDITOR.replace( 'editor1' );
     };
    </script>