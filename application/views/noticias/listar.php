 <div id="wrapper">
    <section id="content">

       <?php if($this->session->flashdata('success')){ ?>

          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
          </div>
       
       <?php }else if($this->session->flashdata('erro')){ ?>
        
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
          </div>
        
       <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span8">
            <h4>Listagem de Notícias</h4>
              <div class="float-sm-right">
                 <form class="form-inline my-2 my-lg-0" action="<?php echo base_url(); ?>noticias/listar/">
                    Pesquisar
                    <input class="form-control mr-sm-2"  name="pesquisa" value="<?php echo $pesquisa?>" type="search" placeholder="" aria-label="Search">
                    <button type="submit" class="btn btn-green btn-medium btn-rounded"> buscar</button>
                  </form>
               </div>
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Título </th>
                        <th> Data de Publicação</th>
                        <th> Ação</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach ($dados as $dado){ 
                        $dado->id; 
                      ?>
                        
                        <tr>
                             <td class="text-left"><?php echo $dado->id; ?></td>
                             <td class="text-left"><?php echo mb_strimwidth($dado->titulo, 0, 50, "..."); ?></td>
                             <td class="text-left"><?php echo $dado->data_noticias; ?></td>
                             <td class="text-left">
                               <a class="icon-eye-open btn btn-blue "href=<?php echo base_url(); ?>site/noticias/<?php echo $dado->id ?> data-toggle="tooltip" title="Vizualiar"></a>
                               <a class="icon-edit btn btn-green" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $dado->id ?>" data-toggle="tooltip" title="Editar"></a>
                               <a class="icon-trash btn btn-red" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/excluir/<?php echo $dado->id ?>" data-toggle="tooltip" title="Excluir"></a>
                             </td>
                                                   

                        </tr>
                        
                    <?php }  ?> 
                    </tbody>
                  </table>

         <center>
            <div class="pagination" id="pagination"> 
              <ul class="pagination pagination-sm no-margin pull-center"> 
                <?php
                if ($total_noticias <> 0){

                        for($p=1, $i=0; $i < $total_noticias; $p++, $i += $itens_per_page):
                            if($page == $p):
                                $tmp[] = "<li class='active'><a href='javascript:;'>".$p."</a></li>";
                            else:
                                $tmp[] = "<li><a href='".base_url()."noticias/listar/".$p."/".$pesquisa."'>".$p."</a></li>";
                            endif;
                        endfor;

                        for($i = count($tmp) - 3; $i > 1; $i--):
                            if(abs($page - $i - 1) > 2):
                                unset($tmp[$i]);
                            endif;
                        endfor;

                        if(count($tmp) > 1):
                            if($page > 1):
                                echo "<li ><a href='".base_url()."noticias/listar/" .($page - 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                            $lastlink = 0;
                            foreach($tmp as $i => $link):
                                if($i > $lastlink + 1):
                                    echo "<li><a href='javascript:;'>...</a></li>";
                                endif;
                                echo $link;
                                $lastlink = $i;
                            endforeach;

                            if($page <= $lastlink):
                                echo "<li ><a href='".base_url()."noticias/listar/" .($page + 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                        endif;

                }else{

                  echo "Notícia não encontrada. Tente Novamente!";
                }

              ?>

              </ul>
            </div>
          </center>  
            </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Notícias</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>noticias">Criar Notícia</a></li>
                  <?php foreach ($dadosContador as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/listar">Listar Notícias</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                </ul>
              </div>
            </aside>
          </div>

        </div>
      </div>
    </section>

  </div>

