    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span12">
            <h2 class="aligncenter">404 Error </h3>
				<p class="aligncenter">
			        Você não possui acesso.
			        Então, você pode <a  href="<?php echo base_url(); ?>site">retornar para o início</a> ou contate a Vision Development para mais informações.
				</p>
			</div>
		</div>
	</div>
	</section>