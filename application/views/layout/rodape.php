    <!-- Footer
 ================================================== -->
    <footer class="footer">
      <div class="container rt">
        <div class="row">

          <div class="span4">
            <div class="widget">
              <h5><address>
			    <i class="icon-home"></i> 
			    <strong>Lions Futebol Clube</strong><br>
				 Rua da Mangabeira, 242<br>
				 Mangabeira, Recife - PE
				</address></h5>
            </div>
          </div>

          <div class="span4">
            <div class="widget">
              <h5><i class="icon-phone"></i> (81) 98865-8843 - (81) 99749-6843<br>
               <i class="icon-envelope-alt"></i> lionsfc@lionsfc.com.br</h5>
            </div>
          </div>

          <div class="span4">
            <div class="widget">
              <ul class="social-network">
                <li><a href="https://www.facebook.com/lionsfem" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-square"></i></a></li>
                <li><a href="https://twitter.com/lionsfeminino" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-square"></i></a></li>
                <li><a href="https://www.instagram.com/lions_feminino/" data-placement="bottom" title="Instagram"><i class="icon-pinterest icon-square"></i></a></li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    </footer>
    <footer class="footer rtcolor">
      <div class="verybottom">
        <div class="container">
          <div class="row">
		    <div class="pull-right hidden-xs rt">
		      <img src="<?php echo base_url(); ?>assents/img/logofooter.png" alt="" /> <b>Lions FC</b>
		    </div>
		    <strong>Copyright &copy; <a href="http://visiondevelopment.com.br/" target="_blank">Vision Development</a></strong> 
          </div>
        </div>
      </div>
    </footer>

  </div>
  <!-- end wrapper -->

  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-48 active"></i></a>
  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?php echo base_url(); ?>assents/js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jcarousel/jquery.jcarousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.fancybox.pack.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.fancybox-media.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/google-code-prettify/prettify.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/portfolio/jquery.quicksand.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/portfolio/setting.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.flexslider.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.nivo.slider.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/modernizr.custom.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.ba-cond.min.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/jquery.slitslider.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/animate.js"></script>
  <script src="<?php echo base_url(); ?>assents/ckeditor/ckeditor.js"></script>
  <script src="<?php echo base_url(); ?>assents/js/bootstrap-datetimepicker.min.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="<?php echo base_url(); ?>assents/js/custom.js"></script>

</body>
</html>