    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assents/menu/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assents/menu/css/menu.css">
  <script type="text/javascript" src="<?php echo base_url(); ?>assents/menu/js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assents/menu/js/function.js"></script>

<body>
  <div id="wrapper">
    <!-- start header -->
    <header>
      <div class="container">
        <div class="row nomargin">
          <div class="span12">
            <div class="headnav">
              <ul>
              <?php if (!$this->session->userdata('usuario')) { ?>

                 <li><a href="#mySignin" data-toggle="modal">Login</a></li>

              <?php }else{ ?>

                 <li><a href="<?php echo base_url(); ?>site/logout" >Sair</a></li>
                 <li class="user-header">

                    <p>
                        <?php echo $this->session->userdata('usuario'); ?> |
                        <small><?php echo $this->session->userdata('perfil_nome'); ?></small>
                    </p>
                 </li>
              <?php } ?>

              </ul>
            </div>
            <!-- Sign in Modal -->
            <div id="mySignin" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div class="modal-body">

                <form class="form-horizontal" action="<?php echo base_url(); ?>Site/processarLogin" method="post">
                  <div class="control-group">
                    <label class="control-label" for="inputText">Login</label>
                    <div class="controls">
                      <input type="text" id="inputText" name="usuario">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label"  for="inputSigninPassword">Senha</label>
                    <div class="controls">
                      <input type="password" id="inputSigninPassword" name="senha">
                    </div>
                  </div>
                  <div class="control-group">
                    <div class="controls">
                      <button type="submit" class="btn">Entrar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- end signin modal -->
          </div>
        </div>
        <div class="row">
          <div class="span4">
            <div class="logo rtlogo">
              <a href="<?php echo base_url(); ?>site"  ><img src="<?php echo base_url(); ?>assents/img/logofooter.png" alt="" class="logo" id="teste"/></a>
                           <a id="menu-toggle" class="button dark" href="#"><i class="icon-reorder icon-2x"></i></a>

            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              
              <div class="navigation">
                <nav>
                  <ul class="nav topnav" id="main-menu">
                    <li class="dropdown active">
                      <a href="<?php echo base_url(); ?>site">Home</a>
                    </li>
                    <li class="dropdown active">
                      <a href="<?php echo base_url(); ?>QSomos">Quem Somos</a>
                    </li>
                    <li class="dropdown active">
                      <a href="<?php echo base_url(); ?>futebol">Futebol Feminino</a>
                    </li>
                    <li class="dropdown active">
                      <a href="<?php echo base_url(); ?>futebol/futebolm">Futebol Masculino</a>
                    </li>
                    <li class="dropdown active">
                      <a href="<?php echo base_url(); ?>calendario">Calendário</a>
                    </li>
                    <li class="dropdown active">
                      <a href="<?php echo base_url(); ?>contato">Contato </a>
                    </li>
                    <?php if ($this->session->userdata('usuario')) { ?>
                    <li class="dropdown active">
                      <a href="#">Administração <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
<!--                         <li><a href="typography.html">Typography</a></li>
                        <li><a href="table.html">Table</a></li>
                        <li><a href="components.html">Components</a></li>
                        <li><a href="animations.html">56 Animations</a></li>
                        <li><a href="icons.html">Icons</a></li> -->
                        <?php if($this->session->userdata('perfil')['NOTICIAS'] == 1){ ?>
                        <li><a href="<?php echo base_url(); ?>noticias">Notícias</a></li>
                        <?php } ?>
                        <?php if($this->session->userdata('perfil')['CALENDARIO'] == 1){ ?>
                        <li><a href="<?php echo base_url(); ?>calendario/calendarioadm">Calendário</a></li>
                        <?php } ?>
                        <?php if($this->session->userdata('perfil')['FUTEBOL'] == 1){ ?>
                        <li><a href="<?php echo base_url(); ?>futebol/futeboladm">Futebol</a></li>
                        <?php } ?>
                        <?php if($this->session->userdata('perfil')['FISIOTERAPEUTA'] == 1){ ?>
                        <li><a href="<?php echo base_url(); ?>fisioterapeuta">Fisioterapeuta</a></li>
                        <?php } ?>
                        <?php if($this->session->userdata('perfil')['USUARIO'] == 1){ ?>
                        <li><a href="<?php echo base_url(); ?>usuarios">Usuário</a></li>
                        <?php } ?>
<!--                         <li class="dropdown"><a href="#">F. Profissional<i class="icon-angle-right"></i></a>
                          <ul class="dropdown-menu sub-menu-level1">
                            <li><a href="index.html">Atleta</a></li>
                            <li><a href="index-alt2.html">Comissão Tec.</a></li>
                          </ul>
                        </li> -->
                      </ul>
                    </li>
                  <?php } ?>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- end header -->



<!--  <div id="wrap"  >
  <header>
    <div class="inner relative">
      <a class="logo" href="http://www.freshdesignweb.com"><img src="images/logo.png" alt="fresh design web"></a>
      <a id="menu-toggle" class="button dark" href="#"><i class="icon-reorder"></i></a>
      <nav id="navigation">
        <ul id="main-menu">
          <li class="current-menu-item"><a href="<?php echo base_url(); ?>site">Home</a></li>
          <li class="parent">
            <a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Features</a>
            <ul class="sub-menu">
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html"><i class="icon-wrench"></i> Elements</a></li>
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html"><i class="icon-credit-card"></i>  Pricing Tables</a></li>
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html"><i class="icon-gift"></i> Icons</a></li>
              <li>
                <a class="parent" href="#"><i class="icon-file-alt"></i> Pages</a>
                <ul class="sub-menu">
                  <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Full Width</a></li>
                  <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Left Sidebar</a></li>
                  <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Right Sidebar</a></li>
                  <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Double Sidebar</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Portfolio</a></li>
          <li class="parent">
            <a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Blog</a>
            <ul class="sub-menu">
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Large Image</a></li>
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Medium Image</a></li>
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Masonry</a></li>
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Double Sidebar</a></li>
              <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Single Post</a></li>
            </ul>
          </li>
          <li><a href="http://www.freshdesignweb.com/responsive-drop-down-menu-jquery-css3-using-icon-symbol.html">Contact</a></li>
        </ul>
      </nav>
      <div class="clear"></div>
    </div>
  </header> 
</div>  -->