<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>LIONS FC</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assents/css/bootstrap.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assents/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assents/css/fancybox/jquery.fancybox.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assents/css/jcarousel.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assents/css/flexslider.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assents/css/slitslider.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assents/css/style.css" rel="stylesheet" />
  <!-- Theme skin -->
  <link id="t-colors" href="<?php echo base_url(); ?>assents/skins/default.css" rel="stylesheet" />
  <!-- boxed bg -->
  <link id="bodybg" href="<?php echo base_url(); ?>assents/bodybg/bg1.css" rel="stylesheet" type="text/css" />
  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assents/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assents/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assents/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assents/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assents/ico/logo.ico" />
  <link href="<?php echo base_url(); ?>assents/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

  <script>                        
     function so_numero(e,args){            
         if (document.all){var evt=event.keyCode;} 
         else{var evt = e.charCode;}  
         var valid_chars = '0123456789';      
         var chr= String.fromCharCode(evt);      
         if (valid_chars.indexOf(chr)>-1 ){return true;} 
         if (valid_chars.indexOf(chr)>-1 || evt < 9){return true;} 
     if (valid_chars.indexOf(chr)>30 || evt <35){return true;}
         return false;   
     }             
  </script>
  <script type="text/javascript">
    function maiuscula(z){
            v = z.value.toUpperCase();
            z.value = v;
        }
  </script>



</head>