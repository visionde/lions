 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
  
 <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span8">
            <div class="widget">
              <h5 class="widgetheading">Criar Notícia</h5>
              <form id="commentform" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/cadastrarUsuarioExe" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span8">
                   <label for="descricao">Nome Completo</label>
                   <input type="text" maxlength="100" class="form-control" id="nome" name="nome" value="<?php echo set_value('nome');?>" maxlength="20">
                   <?php echo form_error('nome');?>
                   <label for="descricao">Usuário</label>
                   <input type="text" maxlength="15" class="form-control" id="usuario" name="usuario" value="<?php echo set_value('usuario');?>" maxlength="20">
                   <?php echo form_error('usuario');?>
                   <label for="descricao">Senha</label>
                   <input type="password" class="form-control" id="senha" name="senha" maxlength="10">
                   <?php echo form_error('senha');?>
                   <label for="descricao">Confirmação de senha</label>
                   <input type="password" class="form-control" id="senha2" name="senha2" maxlength="10">
                   <?php echo form_error('senha2');?><br>

                    <p>
                      <button class="btn btn-green" type="submit">Enviar</button>
                      <a href="<?php echo base_url(); ?>usuarios" class="btn btn-red">Cancelar</a>
                    </p>
                  </div>  
                </div>
              </form>
            </div>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Usuários</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>usuarios/cadastrarUsuario">Criar Usuários</a></li>
                  <?php foreach ($dadosContador as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>usuarios">Listar Usuários</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>
