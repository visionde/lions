   <section id="featured">
      <?php if($this->session->flashdata('erro')){ ?>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata('erro'); ?></h4>                
        </div>
      <?php } ?>
      <!-- start slider -->
      <div id="slider" class="sl-slider-wrapper demo-2" >
        <div class="sl-slider" >
         <?php foreach ($dadosprincipal as $dado){?>
          <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2" >
            <div class="sl-slide-inner" id="content"><center>
              <div class="bg-img bg-img-3">
                 <img style="height:400px;width:1150px;" src="<?php echo base_url(); ?>assents/arquivos/noticias/destaque/<?php echo $dado['destaque_imagem']; ?>" >
              </div></center>

              <a href="<?php echo base_url(); ?>site/noticias/<?php echo $dado['id']; ?>"><p><strong><?php echo $dado['titulo']; ?></strong></font></p></a>
              <blockquote>
                <p>
                </p>
              </blockquote>
            </div>
          </div>
         <?php }?>
        </div>
        <!-- /sl-slider -->
        <nav id="nav-dots" class="nav-dots">
          <span class="nav-dot-current"></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </nav>
      </div>
      <!-- /slider-wrapper -->
      <!-- end slider -->
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="row">
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                   <a href="<?php echo base_url(); ?>QSomos"> <i class="icon-user icon-circled icon-64 active"></i></a>
                  </div>
                  <div class="text">
                    <h6>Quem Somos</h6>
                  </div>
                </div>
              </div>
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                   <a href="<?php echo base_url(); ?>futebol"> <i class="icon-circled icon-64 active">  <img src="http://www.lionsfc.com.br/assents/arquivos/fotos/fem.png" class="img-circle" id="preview" ></i></a>
                  </div>
                  <div class="text">
                    <h6>Futebol Feminino</h6>
                  </div>
                </div>
              </div>
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                   <a href="<?php echo base_url(); ?>futebol/futebolm"> <i class="icon-circled icon-64 active">  <img src="http://www.lionsfc.com.br/assents/arquivos/fotos/mas.png" class="img-circle" id="preview" ></i></a>
                  </div>
                  <div class="text">
                    <h6>Futebol Masculino</h6>
                  </div>
                </div>
              </div>
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                   <a href="<?php echo base_url(); ?>contato"> <i class="icon-envelope-alt icon-circled icon-64 active"></i></a>
                  </div>
                  <div class="text">
                    <h6>Contato</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- divider -->
        <div class="row">
          <div class="span12">
            <div class="solidline">
            </div>
          </div>
        </div>
        <!-- end divider -->
        <!-- Portfolio Projects -->
        <div class="row">
          <div class="span12">
            <h4 class="heading">Noticias</h4>
            <div class="row">
              <section id="projects">



                <ul id="thumbs" class="portfolio">



                <?php foreach ($dados as $dado){?>


                  <li class="item-thumbs span3 design" >
                    <a class="hover-wrap " data-fancybox-group="gallery"  href=<?php echo base_url(); ?>site/noticias/<?php echo $dado['id']; ?>></a>
                    <?php if($dados[0]['destaque_imagem'] != NULL){ ?>
                       <img style="height:270px;width:1600px;" src="<?php echo base_url(); ?>assents/arquivos/noticias/mini/<?php echo $dado['mini_imagem']; ?>" >
                    <?php }else{ ?>
                       <img src="<?php echo base_url(); ?>assents/arquivos/noticias/mini/padrao.png" >
                    <?php } ?>
                    <div class="text aligncenter">
                    <p><?php echo mb_strimwidth($dado['titulo'], 0, 30, "..."); ?></p>
                    <a href="<?php echo base_url(); ?>site/noticias/<?php echo $dado['id']; ?>">Ler Mais</a>
                  </div>
                  </li>


                <?php }?>



                </ul>
              </section>
            </div>
          </div>
        </div>
        <!-- End Portfolio Projects -->
                <!-- divider -->
        <div class="row">
          <div class="span12">
            <div class="solidline">
            </div>
          </div>
        </div>
        
        <!-- divider -->
               <div class="row">
          <div class="span12">
            <ul  class="jcarousel-skin-tango recent-jcarousel clients">
              <li>
              
          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client1.png" class="client-logo" alt="" />
          
              </li>
              <li>

          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client3.png" class="client-logo" alt="" />

              </li>
              <li>

          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client5.png" class="client-logo" alt="" />

              </li>
              <li>

          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client4.jpeg" class="client-logo" alt="" />

              </li>
              <li>

          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client1.png" class="client-logo" alt="" />

              </li>
              <li>

          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client3.png" class="client-logo" alt="" />

              </li>
              <li>
  
          <img src="<?php echo base_url(); ?>assents/img/dummies/clients/client5.png" class="client-logo" alt="" />

              </li>
           
            </ul>
          </div>
        </div>
      </div>
    </section>

 