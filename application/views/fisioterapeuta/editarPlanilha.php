 <div id="wrapper">
    <section id="content">

       <?php if($this->session->flashdata('success')){ ?>

          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
          </div>
       
       <?php }else if($this->session->flashdata('erro')){ ?>
        
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
          </div>
        
       <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span10">
            <h4>Editar Relação de Atletas do Departamento Fisioterapeutico</h4> 
             <form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarPlanilha" method="post" name="comment-form" enctype="multipart/form-data">
                  <table class="table table table-bordered">
                    <thead>
                      <tr>
                        <th> Nome</th>
                        <th> Lesão</th>
                        <th> Treinamento</th>
                        <!-- <th> Ação</th> -->
                      </tr>
                    </thead>
                    <tbody>



<!--                    <?php foreach ($dados as $dado){ 
                        $dado->id; 
                      ?>
                        
                        <tr>


                            <input type="hidden" name="id" value="<?php echo $dado->id;?>">
                             <td class="text-left"><?php echo $dado->nome; ?><input type="hidden" name="nome" value="<?php echo $dado->nome;?>"></td>
                             <td class="text-left"><input type="text"  id="treinamento" name="treinamento"  maxlength="100"  value="<?php echo $dado->treinamento; ?>" onkeyup='maiuscula(this)'></td>
                             <td class="text-left"><input type="text"  id="lesao" name="lesao"  maxlength="100"  value="<?php echo $dado->lesao; ?>" onkeyup='maiuscula(this)'></td>


                        </tr>
                        
                    <?php }  ?>  -->


                 <?php 

                   $result = $dados ;  



                   // for ($i = 0;  $i < count($result); $i++) {

                   //    var_dump($result[$i]->treinamento);

                   // }


                   // echo'<pre>'; var_dump($result); exit();         

                     for ($i = 0;  $i < count($result); $i++) {?>
                          
                        <tr>
                             <input type="hidden" name="id<?php echo $i ?>" value="<?php echo $result[$i]->id;?>">
                             <td class="text-left"><?php echo $result[$i]->nome; ?><input type="hidden" name="nome<?php echo $i ?>" value="<?php echo $result[$i]->nome; ?>"></td>
                             <td class="text-left"><input type="text"  id="lesao<?php echo $i ?>" name="lesao<?php echo $i ?>" value="<?php echo $result[$i]->lesao ?>" maxlength="100" onkeyup='maiuscula(this)'></td>
                             <td class="text-left"><input type="text"  id="treinamento<?php echo $i ?>" value="<?php echo $result[$i]->treinamento ?>" name="treinamento<?php echo $i ?>"  maxlength="100" onkeyup='maiuscula(this)'></td>
            

                        </tr>


                     <?php  } ?>

                    </tbody>
                  </table>
                    <p><br><br><br>

                      <button class="btn btn-green" type="submit">Enviar</button>
                      <a href="<?php echo base_url(); ?>fisioterapeuta/editar" class="btn btn-red">Cancelar</a>
                    </p>
              </form>
            </div>
          <div class="span2">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Fisioterapeuta</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>fisioterapeuta">Criar Planilha</a></li>
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>fisioterapeuta/editar">Editar Planilha</a></li>
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>fisioterapeuta/baixar">Baixar Planilha</a></li>
                </ul>
              </div>
            </aside>
          </div>

        </div>
      </div>
    </section>

  </div>

