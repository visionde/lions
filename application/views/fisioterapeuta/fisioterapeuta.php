 <div id="wrapper">
    <section id="content">

       <?php if($this->session->flashdata('success')){ ?>

          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
          </div>
       
       <?php }else if($this->session->flashdata('erro')){ ?>
        
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
          </div>
        
       <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span10">
            <h4>Criação da Relação de Atletas do Departamento Fisioterapeutico</h4> 
             <form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/relacaoExe" method="post" name="comment-form" enctype="multipart/form-data">
                    <label for="country">MÊS/ANO</label>              
                    <select id="mes" name="mes">
                    <option value="">Selecione</option>
                    <option value="1">JANEIRO</option>
                    <option value="2">FEVEREIRO</option>
                    <option value="3">MARÇO</option>
                    <option value="4">ABRIL</option>
                    <option value="5">MAIO</option>
                    <option value="6">JUNHO</option>
                    <option value="7">JULHO</option>
                    <option value="8">AGOSTO</option>
                    <option value="9">SETEMBRO</option>
                    <option value="10">OUTUBRO</option>
                    <option value="11">NOVEMBRO</option>
                    <option value="12">DEZEMBRO</option>
                  </select>
                  <?php echo form_error('mes');?>
                  <input type="text" name="ano" id="ano" value="<?php echo date('Y'); ?>" >
                  <?php echo form_error('ano');?>
                  <table class="table table table-bordered">
                    <thead>
                      <tr>
                        <th> Nome</th>
                        <th> Lesão</th>
                        <th> Treinamento</th>
                        <!-- <th> Ação</th> -->
                      </tr>
                    </thead>
                    <tbody>

                  <?php 

                   $result = $dados ;              

                     for ($i = 0;  $i < count($result); $i++) {?>
                          
                        <tr>
                             
                             <td class="text-left"><?php echo $result[$i]->nome; ?><input type="hidden" name="nome<?php echo $i ?>" value="<?php echo $result[$i]->nome; ?>"></td>
                             <td class="text-left"><input type="text"  id="lesao<?php echo $i ?>" name="lesao<?php echo $i ?>"  maxlength="100" onkeyup='maiuscula(this)'></td>
                             <td class="text-left"><input type="text"  id="treinamento<?php echo $i ?>" name="treinamento<?php echo $i ?>"  maxlength="100" onkeyup='maiuscula(this)'></td>
            

                        </tr>


                     <?php  } ?>

                    </tbody>
                  </table>
                    <p><br><br><br>

                      <button class="btn btn-green" type="submit">Enviar</button>
                      <a href="<?php echo base_url(); ?>site" class="btn btn-red">Cancelar</a>
                    </p>
              </form>
            </div>
          <div class="span2">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Fisioterapeuta</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>fisioterapeuta">Criar Planilha</a></li>
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>fisioterapeuta/editar">Editar Planilha</a></li>
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>fisioterapeuta/baixar">Baixar Planilha</a></li>
                </ul>
              </div>
            </aside>
          </div>

        </div>
      </div>
    </section>

  </div>

