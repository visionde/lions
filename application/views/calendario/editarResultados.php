 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
 <?php } ?>  


      <div class="container">
        <div class="row">
          <div class="span8">
            <div class="widget">
              <h5 class="widgetheading">Editar Resultados</h5>
              <form action="<?php echo base_url(); ?>calendario/editarResultadosExe" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span2">
                   <input type="hidden" name="id" id="id" value="<?= $dados->id; ?>">

                   <label for="descricao">Time Casa</label>
                      <select class="form-control" id='casa' name="casa" disabled>
                        <option value="">Selecione</option>
                        <?php foreach ($times as $ci) {  $select = '';?>

                       <?php

                        if ($ci->id == $dados->id_time_casa) {
                            $select = 'selected';

                        } ?>


                         <option value="<?php echo $ci->id ?>"<?php echo $select ?>><?php echo $ci->time ?></option>

                        <?php } ?>
                     > </select>
                    <?= form_error('casa'); ?>

                   <label for="descricao">Time Visitante</label>
                      <select class="form-control" id='visitante' name="visitante" disabled>
                        <option value="">Selecione</option>
                        <?php foreach ($times as $ci2) {  $select = ''; ?>

                        <?php

                        if ($ci2->id == $dados->id_time_visitante) {
                            $select = 'selected';

                        } ?>

                         <option value="<?php echo $ci2->id ?>"<?php echo $select ?>><?php echo $ci2->time ?></option>

                        <?php } ?>
                     > </select>
                    <?= form_error('visitante'); ?>


                     <?= form_error('gol_time_visitante'); ?>
                      <?= form_error('gol_time_casa'); ?>

                    <p><br><br><br>
                      <button class="btn btn-green" type="submit">Editar</button>
                      <a href="<?php echo base_url(); ?>calendario/resultados" class="btn btn-red">Cancelar</a>
                    </p>


              </div> 


              <div class="span2">

                    <label for="descricao">Gols Time Casa</label>
                    <input type="text" name="gol_time_casa"  value="<?= $dados->gol_time_casa; ?>">
                   

                    <label for="descricao">Gols Time Visitante</label>
                    <input type="text" name="gol_time_visitante"  value="<?= $dados->gol_time_visitante; ?>">
                   


               </div> 

               <div class="span2">

                    <label for="descricao"> Gols Time Casa (P)</label>
                    <input type="text" name="penalti_time_casa"  value="<?= $dados->penalti_time_casa; ?>">


                    <label for="descricao">Gols Time Visitante (P)</label>
                    <input type="text" name="penalti_time_visitante"  value="<?= $dados->penalti_time_visitante; ?>">

               </div> 




                   
                </div>
              </form>
            </div>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Calendario</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/calendarioadm">Criar Partidas</a></li>
                  <?php foreach ($dadosProximas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/proximasPartidas">Listar Próximas Partidas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosRealizadas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/partidasRealizadas">Listar Partidas Realizadas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosResultado as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/resultados">Resultados</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosRealizadas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/times">Listar Times</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $(function() {
        $('#datetimepicker3').datetimepicker({
          pickDate: false
        });
      });
    </script>


    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>