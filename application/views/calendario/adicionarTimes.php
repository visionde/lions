 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
 <?php } ?>  


      <div class="container">
        <div class="row">
          <div class="span8">
            <div class="widget">
              <h5 class="widgetheading">Adicionar Times</h5>
              <form action="<?php echo base_url(); ?>calendario/adicionarTimesExe" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span8">

                   <label for="descricao">Nome do Time</label>
                   <input type="text" maxlength="15" class="form-control " id="time" name="time" value="<?php echo set_value('time'); ?>" maxlength="20">
                   <?php echo form_error('time');?>


                    <p><br><br><br>

                      <button class="btn btn-green" type="submit">Enviar</button>
                      <a href="<?php echo base_url(); ?>calendario/times" class="btn btn-red">Cancelar</a>
                    </p>
                  </div>  
                </div>
              </form>
            </div>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Calendario</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/calendarioadm">Criar Partidas</a></li>
                  <?php foreach ($dadosProximas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/proximasPartidas">Listar Próximas Partidas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosRealizadas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/partidasRealizadas">Listar Partidas Realizadas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosResultado as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/resultados">Resultados</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosTime as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/times">Listar Times</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $(function() {
        $('#datetimepicker3').datetimepicker({
          pickDate: false
        });
      });
    </script>


    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>