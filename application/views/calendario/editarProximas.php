 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
 <?php } ?>  


      <div class="container">
        <div class="row">
          <div class="span8">
            <div class="widget">
              <h5 class="widgetheading">Editar Proximas Partidas</h5>
              <form action="<?php echo base_url(); ?>calendario/editarProximasExe" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span8">
                   <label for="descricao">Torneio</label>
                   <input type="hidden" name="id" id="id" value="<?= $dados->id; ?>">
                   <input type="text" maxlength="100" class="form-control " id="torneio" name="torneio" value="<?php echo $dados->torneio; ?>" maxlength="20">
                   <?php echo form_error('torneio');?>

                   <label for="descricao">Local da Partida</label>
                   <input type="text" maxlength="50" class="form-control " id="local" name="local" value="<?php echo $dados->local_partida; ?>" maxlength="50">
                   <?php echo form_error('local');?>

                   <label for="descricao">Data da Partida</label>
                   <div id="datetimepicker4" class="input-append ">
                      <input data-format="dd-MM-yyyy" name="data" type="text" value="<?php $data = date("d-m-Y", strtotime($dados->data_partida)); echo $data; ?>"></input>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                   </div>
                   <?php echo form_error('data');?>
                   

                   <label for="descricao">Horário da Partida</label>
                    <div id="datetimepicker3" class="input-append">
                        <input data-format="hh:mm" name="horario" type="text" value="<?php echo $dados->hora_partida; ?>"></input>
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                          </i>
                        </span>
                    </div>
                   <?php echo form_error('horario');?>

                   <label for="descricao">Time Casa</label>
                      <select class="form-control" id='casa' name="casa">
                        <option value="">Selecione</option>
                        <?php foreach ($times as $ci) {  $select = '';?>

                       <?php

                        if ($ci->id == $dados->id_time_casa) {
                            $select = 'selected';

                        } ?>


                         <option value="<?php echo $ci->id ?>"<?php echo $select ?>><?php echo $ci->time ?></option>

                        <?php } ?>
                     > </select>
                    <?= form_error('casa'); ?>

                   <label for="descricao">Time Visitante</label>
                      <select class="form-control" id='visitante' name="visitante">
                        <option value="">Selecione</option>
                        <?php foreach ($times as $ci2) {  $select = ''; ?>

                        <?php

                        if ($ci2->id == $dados->id_time_visitante) {
                            $select = 'selected';

                        } ?>

                         <option value="<?php echo $ci2->id ?>"<?php echo $select ?>><?php echo $ci2->time ?></option>

                        <?php } ?>
                     > </select>
                    <?= form_error('visitante'); ?>




                    <p><br><br><br>

                      <button class="btn btn-green" type="submit">Editar</button>
                      <a href="<?php echo base_url(); ?>calendario/proximasPartidas" class="btn btn-red">Cancelar</a>
                    </p>
                  </div>  
                </div>
              </form>
            </div>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Calendario</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/calendarioadm">Criar Partidas</a></li>
                  <?php foreach ($dadosProximas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/proximasPartidas">Listar Próximas Partidas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosRealizadas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/partidasRealizadas">Listar Partidas Realizadas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosResultado as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/resultados">Resultados</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosTime as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/times">Listar Times</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $(function() {
        $('#datetimepicker3').datetimepicker({
          pickDate: false
        });
      });
    </script>


    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>