 <div id="wrapper">
    <section id="content">

       <?php if($this->session->flashdata('success')){ ?>

          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
          </div>
       
       <?php }else if($this->session->flashdata('erro')){ ?>
        
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
          </div>
        
       <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span8">
            <h4>Listar Times</h4>
              <div class="float-sm-right">
                 <form class="form-inline my-2 my-lg-0" action="<?php echo base_url(); ?>calendario/times/">
                    Pesquisar
                    <input class="form-control mr-sm-2"  name="pesquisa" value="<?php echo $pesquisa?>" type="search" placeholder="Buscar por times" aria-label="Search">
                    <button type="submit" class="btn btn-green btn-medium btn-rounded" > buscar</button>
                  </form>

               </div>
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Time </th>
                        <th> </th>
                        <th> <a href="<?php echo base_url(); ?>calendario/adicionarTimes"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"> Adicionar</button></a></th>
                        <!-- <th> Ação</th> -->
                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach ($dados as $dado){ 
                        $dado->id; 
                      ?>
                        
                        <tr>
                             <td class="text-left"><?php echo $dado->id; ?></td>
                             <td class="text-left"><?php echo $dado->time; ?></td>
                             <td class="text-left">
                              <a class="icon-edit btn btn-green" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarTimes/<?php echo $dado->id ?>" data-toggle="tooltip" title="Editar"></a>
<!--                                <a class="icon-trash btn btn-red" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/excluirTimes/<?php echo $dado->id ?>" data-toggle="tooltip" title="Excluir"></a> -->
                             </td>
                                                   

                        </tr>
                        
                    <?php }  ?> 
                    </tbody>
                  </table>

         <center>
            <div class="pagination" id="pagination"> 
              <ul class="pagination pagination-sm no-margin pull-center"> 
                <?php
                if ($total <> 0){

                        for($p=1, $i=0; $i < $total; $p++, $i += $itens_per_page):
                            if($page == $p):
                                $tmp[] = "<li class='active'><a href='javascript:;'>".$p."</a></li>";
                            else:
                                $tmp[] = "<li><a href='".base_url()."calendario/times/".$p."/".$pesquisa."'>".$p."</a></li>";
                            endif;
                        endfor;

                        for($i = count($tmp) - 3; $i > 1; $i--):
                            if(abs($page - $i - 1) > 2):
                                unset($tmp[$i]);
                            endif;
                        endfor;

                        if(count($tmp) > 1):
                            if($page > 1):
                                echo "<li ><a href='".base_url()."calendario/times/" .($page - 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                            $lastlink = 0;
                            foreach($tmp as $i => $link):
                                if($i > $lastlink + 1):
                                    echo "<li><a href='javascript:;'>...</a></li>";
                                endif;
                                echo $link;
                                $lastlink = $i;
                            endforeach;

                            if($page <= $lastlink):
                                echo "<li ><a href='".base_url()."calendario/times/" .($page + 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                        endif;

                }else{

                  echo "Time não encontrado. Tente Novamente!";
                }

              ?>

              </ul>
            </div>
          </center>  
            </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Calendario</h5>
                <ul class="cat">
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/calendarioadm">Criar Partidas</a></li>
                  <?php foreach ($dadosProximas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/proximasPartidas">Listar Próximas Partidas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosRealizadas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/partidasRealizadas">Listar Partidas Realizadas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosResultado as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/resultados">Resultados</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($dadosTime as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>calendario/times">Listar Times</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                </ul>
              </div>
            </aside>
          </div>

        </div>
      </div>
    </section>

  </div>



