    <section id="content">
      <div class="container">
        <div class="row">
        <div class="span12">
            <h4 class="heading">Goleiros</h4>
            <div class="row">
              <section id="projects">



                <ul id="thumbs" class="portfolio">


                <?php foreach ($dadoselencoGoleiro as $dado){?>


                  <li class="item-thumbs span3 design" > 
                    <img src="<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>" class="img-circle" id="preview" style="height:200px;width:200px; ">
                    <div class="text aligncenter">
                    <p><?php 

                    if($dado['apelido'] == NULL){

                      $partes = explode(' ',  $dado['nome']);
                      $primeiroNome = array_shift($partes);
                      $ultimoNome = array_pop($partes);

                      echo $primeiroNome.' '.$ultimoNome; 
                    }else{
                      echo $dado['apelido'];
                    }

                      ?>
                      
                      <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                      <a href="#futebol<?php echo $dado['id']; ?>" data-toggle="modal"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"></button></a>

                     <?php } ?>

                    </p>

                  </div>
                  </li>
               <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                <!-- Sign in Modal -->
                  <div id="futebol<?php echo $dado['id']; ?>" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                        <center><img src='<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>' height='100' width='100'></br></br></center>
                        <center><img src='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' height='100' width='100'></br></br>
                        
                        <?php if($dado['foto_doc']<> NULL) { ?> 
                        <a href='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' target='_blank'>Ver Documento</a></center>
                        <?php } ?>
                          <table class="table table-bordered">
 
                          <tbody>
                              
                            <tr>
                              <td>Nome Completo</td>
                              <td><center><?php echo $dado['nome']; ?></td>
                            </tr>
                            <tr>
                              <td>CPF</td>
                              <td><center><?php echo $dado['cpf']; ?></td>
                            </tr>
                            <tr>
                              <td>RG</td>
                              <td><center><?php echo $dado['rg']; ?></td>
                            </tr>
                            <tr>
                             <td>Data de Nascimento</td>
                              <td><center><?php echo $dado['data_nascimento']; ?></td>
                            </tr>
                           <td>Celular</td>
                              <td><center><?php echo $dado['celular']; ?></td>
                            </tr>
                           <td>Nome da Mãe</td>
                              <td><center><?php echo $dado['nomem']; ?></td>
                            </tr>
                           <td>Nome da Pai</td>
                              <td><center><?php echo $dado['nomep']; ?></td>
                            </tr>
                           <td>Endereço</td>
                              <td><center><?php echo $dado['ende']; ?></td>
                            </tr>
                            </tr>
                           <td>Status</td>
                              <td><center><?php echo $dado['status']; ?></td>
                            </tr>
                           <td>Posição</td>
                              <td><center><?php echo $dado['posicao']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                  </div>
                <!-- end signin modal -->
              <?php } ?>

            <?php }?>

                </ul>
              </section>
            </div>
          </div>
          <div class="span12">
            <h4 class="heading">Defensores</h4>
            <div class="row">
              <section id="projects">



                <ul id="thumbs" class="portfolio">


                <?php foreach ($dadoselencoDefensores as $dado){?>


                
                  <li class="item-thumbs span3 design" > 
                    <img src="<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>" class="img-circle" id="preview" style="height:200px;width:200px; ">
                    <div class="text aligncenter">
                    <p><?php 

                    if($dado['apelido'] == NULL){

                      $partes = explode(' ',  $dado['nome']);
                      $primeiroNome = array_shift($partes);
                      $ultimoNome = array_pop($partes);

                      echo $primeiroNome.' '.$ultimoNome; 
                    }else{
                      echo $dado['apelido'];
                    }

                      ?>
                      
                      <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                      <a href="#futebol<?php echo $dado['id']; ?>" data-toggle="modal"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"></button></a>

                     <?php } ?>

                    </p>

                  </div>
                  </li>
               <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                <!-- Sign in Modal -->
                  <div id="futebol<?php echo $dado['id']; ?>" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                        <center><img src='<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>' height='100' width='100'></br></br></center>
                        <center><img src='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' height='100' width='100'></br></br>
                        
                        <?php if($dado['foto_doc']<> NULL) { ?> 
                        <a href='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' target='_blank'>Ver Documento</a></center>
                        <?php } ?>
                          <table class="table table-bordered">
 
                          <tbody>
                          
                            <tr>
                              <td>Nome Completo</td>
                              <td><center><?php echo $dado['nome']; ?></td>
                            </tr>
                            <tr>
                              <td>CPF</td>
                              <td><center><?php echo $dado['cpf']; ?></td>
                            </tr>
                            <tr>
                              <td>RG</td>
                              <td><center><?php echo $dado['rg']; ?></td>
                            </tr>
                            <tr>
                             <td>Data de Nascimento</td>
                              <td><center><?php echo $dado['data_nascimento']; ?></td>
                            </tr>
                           <td>Celular</td>
                              <td><center><?php echo $dado['celular']; ?></td>
                            </tr>
                           <td>Nome da Mãe</td>
                              <td><center><?php echo $dado['nomem']; ?></td>
                            </tr>
                           <td>Nome da Pai</td>
                              <td><center><?php echo $dado['nomep']; ?></td>
                            </tr>
                           <td>Endereço</td>
                              <td><center><?php echo $dado['ende']; ?></td>
                            </tr>
                            </tr>
                           <td>Status</td>
                              <td><center><?php echo $dado['status']; ?></td>
                            </tr>
                           <td>Posição</td>
                              <td><center><?php echo $dado['posicao']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                  </div>
                <!-- end signin modal -->
              <?php } ?>


                <?php }?>

                </ul>
              </section>
            </div>
          </div>
      
          <div class="span12">
            <h4 class="heading">Meias</h4>
            <div class="row">
              <section id="projects">



                <ul id="thumbs" class="portfolio">


                <?php foreach ($dadoselencoMeia as $dado){?>


                 
                  <li class="item-thumbs span3 design" > 
                    <img src="<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>" class="img-circle" id="preview" style="height:200px;width:200px; ">
                    <div class="text aligncenter">
                    <p><?php 

                    if($dado['apelido'] == NULL){

                      $partes = explode(' ',  $dado['nome']);
                      $primeiroNome = array_shift($partes);
                      $ultimoNome = array_pop($partes);

                      echo $primeiroNome.' '.$ultimoNome; 
                    }else{
                      echo $dado['apelido'];
                    }

                      ?>
                      
                      <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                      <a href="#futebol<?php echo $dado['id']; ?>" data-toggle="modal"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"></button></a>

                     <?php } ?>

                    </p>

                  </div>
                  </li>
               <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                <!-- Sign in Modal -->
                  <div id="futebol<?php echo $dado['id']; ?>" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                        <center><img src='<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>' height='100' width='100'></br></br></center>
                        <center><img src='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' height='100' width='100'></br></br>
                        
                        <?php if($dado['foto_doc']<> NULL) { ?> 
                        <a href='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' target='_blank'>Ver Documento</a></center>
                        <?php } ?>
                          <table class="table table-bordered">
 
                          <tbody>
                          
                            <tr>
                              <td>Nome Completo</td>
                              <td><center><?php echo $dado['nome']; ?></td>
                            </tr>
                            <tr>
                              <td>CPF</td>
                              <td><center><?php echo $dado['cpf']; ?></td>
                            </tr>
                            <tr>
                              <td>RG</td>
                              <td><center><?php echo $dado['rg']; ?></td>
                            </tr>
                            <tr>
                             <td>Data de Nascimento</td>
                              <td><center><?php echo $dado['data_nascimento']; ?></td>
                            </tr>
                           <td>Celular</td>
                              <td><center><?php echo $dado['celular']; ?></td>
                            </tr>
                           <td>Nome da Mãe</td>
                              <td><center><?php echo $dado['nomem']; ?></td>
                            </tr>
                           <td>Nome da Pai</td>
                              <td><center><?php echo $dado['nomep']; ?></td>
                            </tr>
                           <td>Endereço</td>
                              <td><center><?php echo $dado['ende']; ?></td>
                            </tr>
                            </tr>
                           <td>Status</td>
                              <td><center><?php echo $dado['status']; ?></td>
                            </tr>
                           <td>Posição</td>
                              <td><center><?php echo $dado['posicao']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                  </div>
                <!-- end signin modal -->
              <?php } ?>

                <?php }?>

                </ul>
              </section>
            </div>
          </div>
          <div class="span12">
            <h4 class="heading">Atacantes</h4>
            <div class="row">
              <section id="projects">



                <ul id="thumbs" class="portfolio">


                <?php foreach ($dadoselencoAtacante as $dado){?>


                
                  <li class="item-thumbs span3 design" > 
                    <img src="<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>" class="img-circle" id="preview" style="height:200px;width:200px; ">
                    <div class="text aligncenter">
                    <p><?php 

                    if($dado['apelido'] == NULL){

                      $partes = explode(' ',  $dado['nome']);
                      $primeiroNome = array_shift($partes);
                      $ultimoNome = array_pop($partes);

                      echo $primeiroNome.' '.$ultimoNome; 
                    }else{
                      echo $dado['apelido'];
                    }

                      ?>
                      
                      <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                      <a href="#futebol<?php echo $dado['id']; ?>" data-toggle="modal"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"></button></a>

                     <?php } ?>

                    </p>

                  </div>
                  </li>
               <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                <!-- Sign in Modal -->
                  <div id="futebol<?php echo $dado['id']; ?>" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                        <center><img src='<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>' height='100' width='100'></br></br></center>
                        <center><img src='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' height='100' width='100'></br></br>
                        
                        <?php if($dado['foto_doc']<> NULL) { ?> 
                        <a href='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' target='_blank'>Ver Documento</a></center>
                        <?php } ?>
                          <table class="table table-bordered">
 
                          <tbody>
                          
                            <tr>
                              <td>Nome Completo</td>
                              <td><center><?php echo $dado['nome']; ?></td>
                            </tr>
                            <tr>
                              <td>CPF</td>
                              <td><center><?php echo $dado['cpf']; ?></td>
                            </tr>
                            <tr>
                              <td>RG</td>
                              <td><center><?php echo $dado['rg']; ?></td>
                            </tr>
                            <tr>
                             <td>Data de Nascimento</td>
                              <td><center><?php echo $dado['data_nascimento']; ?></td>
                            </tr>
                           <td>Celular</td>
                              <td><center><?php echo $dado['celular']; ?></td>
                            </tr>
                           <td>Nome da Mãe</td>
                              <td><center><?php echo $dado['nomem']; ?></td>
                            </tr>
                           <td>Nome da Pai</td>
                              <td><center><?php echo $dado['nomep']; ?></td>
                            </tr>
                           <td>Endereço</td>
                              <td><center><?php echo $dado['ende']; ?></td>
                            </tr>
                            </tr>
                           <td>Status</td>
                              <td><center><?php echo $dado['status']; ?></td>
                            </tr>
                           <td>Posição</td>
                              <td><center><?php echo $dado['posicao']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                  </div>
                <!-- end signin modal -->
              <?php } ?>


                <?php }?>

                </ul>
              </section>
            </div>
          </div>


        <div class="span12">
            <h4 class="heading">Comissão Técnica</h4>
            <div class="row">
              <section id="projects">



                <ul id="thumbs" class="portfolio">


                <?php foreach ($dadoscomissao as $dado){?>


                  <li class="item-thumbs span3 design" > 
                    <img src="<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>" class="img-circle" id="preview" style="height:200px;width:200px; ">
                    <div class="text aligncenter">
                    <p><?php 

                    if($dado['apelido'] == NULL){

                      $partes = explode(' ',  $dado['nome']);
                      $primeiroNome = array_shift($partes);
                      $ultimoNome = array_pop($partes);

                      echo $primeiroNome.' '.$ultimoNome; 
                    }else{
                      echo $dado['apelido'];
                    }

                      ?>
                      
                      <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                      <a href="#futebol<?php echo $dado['id']; ?>" data-toggle="modal"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"></button></a>

                     <?php } ?>

                    </p>

                  </div>
                  </li>
               <?php if ($this->session->userdata('perfil_nome') == "ADMINISTRADOR" or $this->session->userdata('perfil_nome') == "GERENTE") { ?>
                <!-- Sign in Modal -->
                  <div id="futebol<?php echo $dado['id']; ?>" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">

                        <center><img src='<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dado['foto']; ?>' height='100' width='100'></br></br></center>
                        <center><img src='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' height='100' width='100'></br></br>
                        
                        <?php if($dado['foto_doc']<> NULL) { ?> 
                        <a href='<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dado['foto_doc']; ?>' target='_blank'>Ver Documento</a></center>
                        <?php } ?>
                          <table class="table table-bordered">
 
                          <tbody>
                          
                            <tr>
                              <td>Nome Completo</td>
                              <td><center><?php echo $dado['nome']; ?></td>
                            </tr>
                            <tr>
                              <td>CPF</td>
                              <td><center><?php echo $dado['cpf']; ?></td>
                            </tr>
                            <tr>
                              <td>RG</td>
                              <td><center><?php echo $dado['rg']; ?></td>
                            </tr>
                            <tr>
                             <td>Data de Nascimento</td>
                              <td><center><?php echo $dado['data_nascimento']; ?></td>
                            </tr>
                           <td>Celular</td>
                              <td><center><?php echo $dado['celular']; ?></td>
                            </tr>
                           <td>Nome da Mãe</td>
                              <td><center><?php echo $dado['nomem']; ?></td>
                            </tr>
                           <td>Nome da Pai</td>
                              <td><center><?php echo $dado['nomep']; ?></td>
                            </tr>
                           <td>Endereço</td>
                              <td><center><?php echo $dado['ende']; ?></td>
                            </tr>
                            </tr>
                           <td>Status</td>
                              <td><center><?php echo $dado['status']; ?></td>
                            </tr>
                           <td>Posição</td>
                              <td><center><?php echo $dado['posicao']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                  </div>
                <!-- end signin modal -->
              <?php } ?>


                <?php }?>

                </ul>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>