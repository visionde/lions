 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
  
 <?php } ?>  

      <div class="container">
        <div class="row">
                   <div class="span8">
            <h4>Listagem de Comissão Técnica Antiga</h4>
              <div class="float-sm-right">
                 <form class="form-inline my-2 my-lg-0" action="<?php echo base_url(); ?>futebol/comissaoAntiga/">
                    Pesquisar
                    <input class="form-control mr-sm-2"  name="pesquisa" value="<?php echo $pesquisa?>" type="search" placeholder="Buscar por Funcionario" aria-label="Search">
                    <button type="submit" class="btn btn-green btn-medium btn-rounded"> buscar</button> 
                  </form>
               </div>
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th> Nome </th>
                        <th> Time </th>
                        <th> Posição </th>
                        <th> Status </th>

                    </thead>
                    <tbody>
                     <?php foreach ($dados as $dado){ 
                        $dado->id; 
                      ?>
                        
                        <tr>
                             <td class="text-left"><?php echo $dado->nome; ?></td>
                             <td class="text-left"><?php echo $dado->time; ?></td>
                             <td class="text-left"><?php echo $dado->status; ?></td>
                             <td class="text-left"><?php echo $dado->posicao; ?></td>
                             <td class="text-left">
                               <a class="icon-check btn btn-blue" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/ativarComissaoAntiga/<?php echo $dado->id ?>" data-toggle="tooltip" title="ativar"></a>
                               <a class=" icon-zoom-in btn btn-green" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/vizualizarComissaoAntiga/<?php echo $dado->id ?>" data-toggle="tooltip" title="vizualizar"></a>
                             </td>
                                                   

                        </tr>
                        
                    <?php }  ?> 
                    </tbody>
                  </table>

         <center>
            <div class="pagination" id="pagination"> 
              <ul class="pagination pagination-sm no-margin pull-center"> 
                <?php
                if ($total <> 0){

                        for($p=1, $i=0; $i < $total; $p++, $i += $itens_per_page):
                            if($page == $p):
                                $tmp[] = "<li class='active'><a href='javascript:;'>".$p."</a></li>";
                            else:
                                $tmp[] = "<li><a href='".base_url()."futebol/comissaoAntiga/".$p."/".$pesquisa."'>".$p."</a></li>";
                            endif;
                        endfor;

                        for($i = count($tmp) - 3; $i > 1; $i--):
                            if(abs($page - $i - 1) > 2):
                                unset($tmp[$i]);
                            endif;
                        endfor;

                        if(count($tmp) > 1):
                            if($page > 1):
                                echo "<li ><a href='".base_url()."futebol/comissaoAntiga/" .($page - 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                            $lastlink = 0;
                            foreach($tmp as $i => $link):
                                if($i > $lastlink + 1):
                                    echo "<li><a href='javascript:;'>...</a></li>";
                                endif;
                                echo $link;
                                $lastlink = $i;
                            endforeach;

                            if($page <= $lastlink):
                                echo "<li ><a href='".base_url()."futebol/comissaoAntiga/" .($page + 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                        endif;

                }else{

                  echo "Jogadora não encontrada. Tente Novamente!";
                }

              ?>

              </ul>
            </div>
          </center>  
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Futebol</h5>
                <ul class="cat">
                  <?php foreach ($contadorAtletas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/futeboladm">Jogadores</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissao as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissao">Comissão Técnica</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissaoAntiga as $dado){?>   
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissaoAntiga">Comissão Técnica Antiga</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorAtletasAntigas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/AtletaAntigas">Jogadores Antigas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/baixar">Baixar Planilha</a></li>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

<script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
    });
  });
</script>


 <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>