 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
  
 <?php } ?>  

      <div class="container">
        <div class="row">
                   <div class="span9">
            <h4>Listagem</h4>
              <div class="float-sm-right">
                 <form class="form-inline my-2 my-lg-0" action="<?php echo base_url(); ?>futebol/futeboladm/">
                    Pesquisar
                    <input class="form-control mr-sm-2"  name="pesquisa" value="<?php echo $pesquisa?>" type="search" placeholder="Buscar" aria-label="Search">
                    <button type="submit" class="btn btn-green btn-medium btn-rounded"> buscar</button> 
                  </form>
               </div>
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th> Nome </th>
                        <th> Time </th>
                        <th> Status </th>
                        <th> Posição </th>
                        <th></th>
                        <th> <a href="#" data-toggle="modal" data-target=".adicionar"><button type="button" class="btn btn-green btn-medium btn-rounded icon-plus btn btn-blue"> Adicionar</button></a></th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach ($dados as $dado){ 
                        $dado->id; 
                      ?>
                        
                        <tr>
                             <td class="text-left"><?php echo $dado->nome; ?></td>
                             <td class="text-left"><?php echo $dado->time; ?></td>
                             <td class="text-left"><?php echo $dado->status; ?></td>
                             <td class="text-left"><?php echo $dado->posicao; ?></td>
                             <td class="text-left">
                              <a class="icon-edit btn btn-green" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarAtleta/<?php echo $dado->id ?>/<?php echo $dado->time ?>" data-toggle="tooltip" title="Editar"></a>
                               <a class="icon-trash btn btn-red" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/excluirAtleta/<?php echo $dado->id ?>" data-toggle="tooltip" title="Excluir"></a>

                               <a class="icon-print btn btn-blue" target="_blank"  href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/contrato/<?php echo $dado->id ?>" data-toggle="tooltip" title="Contrato"></a>

                               <?php if ( $dado->foto_doc) { ?>
                                  <a class="icon-qrcode btn btn-yellow" target="_blank"  href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/generate_qrcode/<?php echo $dado->id ?>" data-toggle="tooltip" title="Contrato"></a>
                               <?php } ?>

                             </td>
                                                   

                        </tr>
                        
                    <?php }  ?> 
                    </tbody>
                  </table>

         <center>
            <div class="pagination" id="pagination"> 
              <ul class="pagination pagination-sm no-margin pull-center"> 
                <?php
                if ($total <> 0){

                        for($p=1, $i=0; $i < $total; $p++, $i += $itens_per_page):
                            if($page == $p):
                                $tmp[] = "<li class='active'><a href='javascript:;'>".$p."</a></li>";
                            else:
                                $tmp[] = "<li><a href='".base_url()."futebol/futeboladm/".$p."/".$pesquisa."'>".$p."</a></li>";
                            endif;
                        endfor;

                        for($i = count($tmp) - 3; $i > 1; $i--):
                            if(abs($page - $i - 1) > 2):
                                unset($tmp[$i]);
                            endif;
                        endfor;

                        if(count($tmp) > 1):
                            if($page > 1):
                                echo "<li ><a href='".base_url()."futebol/futeboladm/" .($page - 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                            $lastlink = 0;
                            foreach($tmp as $i => $link):
                                if($i > $lastlink + 1):
                                    echo "<li><a href='javascript:;'>...</a></li>";
                                endif;
                                echo $link;
                                $lastlink = $i;
                            endforeach;

                            if($page <= $lastlink):
                                echo "<li ><a href='".base_url()."futebol/futeboladm/" .($page + 1)."/".$pesquisa."'>«</a></li>";
                            endif;

                        endif;

                }else{

                  echo "Jogadora não encontrada. Tente Novamente!";
                }

              ?>

              </ul>
            </div>
          </center>  
          </div>
          <div class="span3">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Futebol</h5>
                <ul class="cat">
                  <?php foreach ($contadorAtletas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/futeboladm">Jogadores</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissao as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissao">Comissão Técnica</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissaoAntiga as $dado){?>   
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissaoAntiga">Comissão Técnica Antiga</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorAtletasAntigas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/AtletaAntigas">Jogadores Antigas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/baixar">Baixar Planilha</a></li>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>


<div class="modal fade adicionar" data-easein="flipYIn" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <!-- <h4 class="modal-title">Qual Time ? </h4> -->
        </div>
        <div class="modal-body">
         <table class="stable" width="100%">
             <tbody>
              <center>
                <a href="<?php echo base_url(); ?>futebol/adicionarAtleta">            
                  <img src="<?php echo base_url(); ?>assents/arquivos/fotos/padrao.png" id="foto-cliente" width="150" height="190">
                </a>
                &nbsp; &nbsp; &nbsp; 
                <a href="<?php echo base_url(); ?>futebol/adicionarAtletaM">            
                  <img src="<?php echo base_url(); ?>assents/arquivos/fotos/padraom.jpg" id="foto-cliente" width="150" height="190">
                </a>
              </center>

            </tbody>
         </table>
    
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
    });
  });
</script>


 <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>