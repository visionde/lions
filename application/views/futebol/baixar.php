 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
 <?php } ?>  


      <div class="container">
        <div class="row">
          <div class="span9">
            <div class="widget">
              <h5 class="widgetheading">Baixar Relação de Atletas</h5>
              <form action="<?php echo base_url(); ?>futebol/gerarPlanilha" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span8">
                    <label for="country">TIME/TIPO/STATUS</label>              
                    <select id="time" name="time">
                    <option value="">Selecione</option>
                    <option value="F">Feminino</option>
                    <option value="M">Masculino</option>
                    </select>
                    <?php echo form_error('time');?>
                    <select id="tipo" name="tipo">
                    <option value="">Selecione</option>
                    <option value="1">Ativo</option>
                    <option value="0">Antiga</option>
                    </select>
                    <?php echo form_error('tipo');?>
                    <select id="status" name="status">
                    <option value="">Selecione</option>
                    <option value="Atleta">Atleta</option>
                    <option value="Comissao">Comissao</option>
                    </select>
                    <?php echo form_error('status');?>



                    <p><br><br><br>

                      <button class="btn btn-green" type="submit">Baixar</button>
                      <a href="<?php echo base_url(); ?>futebol" class="btn btn-red">Cancelar</a>
                    </p>
                  </div>  
                </div>
              </form>
            </div>
          </div>
          <div class="span3">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Futebol</h5>
                <ul class="cat">
                  <?php foreach ($contadorAtletas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/futeboladm">Jogadores</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissao as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissao">Comissão Técnica</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissaoAntiga as $dado){?>   
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissaoAntiga">Comissão Técnica Antiga</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorAtletasAntigas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/AtletaAntigas">Jogadores Antigas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/baixar">Baixar Planilha</a></li>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $(function() {
        $('#datetimepicker3').datetimepicker({
          pickDate: false
        });
      });
    </script>


    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>