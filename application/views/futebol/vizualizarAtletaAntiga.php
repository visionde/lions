 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
  
 <?php } ?>  

      <div class="container">
        <div class="row">
                   <div class="span8">
            <h4><?php echo $dados->nome; ?></h4>

                        <table class="table table-bordered">

                        <tbody>
                            
<!--                           <tr>
                            <td>Nome Completo</td>
                            <td><center><?php echo $dados->nome; ?></td>
                          </tr> -->
                          <tr>
                            <td>CPF</td>
                            <td><center><?php echo $dados->cpf; ?></td>
                          </tr>
                          <tr>
                            <td>RG</td>
                            <td><center><?php echo $dados->rg; ?></td>
                          </tr>
                          <tr>
                           <td>Data de Nascimento</td>
                            <td><center><?php echo $dados->data_nascimento; ?></td>
                          </tr>
                         <td>Celular</td>
                            <td><center><?php echo $dados->celular; ?></td>
                          </tr>
                         <td>Nome da Mãe</td>
                            <td><center><?php echo $dados->nomem; ?></td>
                          </tr>
                         <td>Nome da Pai</td>
                            <td><center><?php echo $dados->nomep; ?></td>
                          </tr>
                         <td>Endereço</td>
                            <td><center><?php echo $dados->ende; ?></td>
                          </tr>
                          </tr>
                         <td>Status</td>
                            <td><center><?php echo $dados->status; ?></td>
                          </tr>
                         <td>Posição</td>
                            <td><center><?php echo $dados->posicao; ?></td>
                          </tr>
                        </tbody>
                      </table>


          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Futebol</h5>
                <ul class="cat">
                  <?php foreach ($contadorAtletas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/futeboladm">Jogadores</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissao as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissao">Comissão Técnica</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissaoAntiga as $dado){?>   
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissaoAntiga">Comissão Técnica Antiga</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorAtletasAntigas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/AtletaAntigas">Jogadores Antigas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/baixar">Baixar Planilha</a></li>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

<script type="text/javascript">
  $(function() {
    $('#datetimepicker3').datetimepicker({
      pickDate: false
    });
  });
</script>


 <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>