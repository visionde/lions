 
 <style type="text/css">
   
input[type=text], select {
  width: 70%;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=text] {

  padding: 15px 20px;


}

.input-append .add-on, .input-prepend .add-on{

  padding: 9px 5px;

}

</style>
 </style>
 <section id="content">
 <?php if($this->session->flashdata('success')){ ?>

    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('success'); ?></h6>               
    </div>
 
 <?php }else if($this->session->flashdata('erro')){ ?>
  
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h6><i class="icon fa fa-check"></i> <?php echo $this->session->flashdata('erro'); ?></h6>               
    </div>
  
 <?php } ?>  

      <div class="container">
        <div class="row">
          <div class="span8">
            <div class="widget">
              <h5 class="widgetheading">Adicionar Jogador </h5>
              <form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarAtletaExe" method="post" name="comment-form" enctype="multipart/form-data">
                <div class="row">
                  <div class="span8">
                  <input type="hidden" name="id" id="id" value="<?= $dados->id; ?>">
                  <label for="nome">Selecionar Foto</label>
                  <a href="#"><img src="<?php echo base_url(); ?>assents/arquivos/fotos/<?php echo $dados->foto; ?>" height="190" width="150" id="foto-cliente"></a><br>
                  <input type="file" name="foto" id="foto"><br><br>
                  <input type="hidden" name="time" id="time" value="M">

                  <label for="nome">Selecionar Foto Documento</label>
                  <a href="#"><img src="<?php echo base_url(); ?>assents/arquivos/documentos/<?php echo $dados->foto_doc; ?>" height="190" width="150" id="foto_documento"></a><br>
                  <input type="file" name="foto_doc" id="foto_doc"><br><br>

                  <label for="fname">Nome Completo</label>
                  <input type="text"  id="nome" name="nome" value="<?php echo $dados->nome; ?>" maxlength="100" onkeyup='maiuscula(this)'>
                  <?php echo form_error('nome');?>

                  <label for="fname">Apelido</label>
                  <input type="text"  id="apelido" name="apelido" value="<?php echo $dados->apelido; ?>" maxlength="100" onkeyup='maiuscula(this)'>
                  <?php echo form_error('apelido');?>

                  <label for="country">Posição</label>
                  <select id="posicao" name="posicao">

                      <option value="" <?php echo $dados->posicao=='Selecione'?'selected':'';?> >Selecione</option>
                      <option value="Goleiro" <?php echo $dados->posicao=='Goleiro'?'selected':'';?> >Goleiro</option>
                      <option value="Defensor" <?php echo $dados->posicao=='Defensor'?'selected':'';?> >Defensor</option>
                      <option value="Meia" <?php echo $dados->posicao=='Meia'?'selected':'';?> >Meia</option>
                      <option value="Atacante" <?php echo $dados->posicao=='Atacante'?'selected':'';?> >Atacante</option>

                  </select>
                  <?php echo form_error('posicao');?>

                  <label for="fname">CPF</label>
                  <input type="text" id="cpf" name="cpf" value="<?php echo $dados->cpf; ?>" maxlength="11" onKeyPress='return so_numero(event);'>
                  <?php echo form_error('cpf');?>

                  <label for="fname">RG</label>
                  <input type="text"  id="rg" name="rg" value="<?php echo $dados->rg; ?>" maxlength="14" onKeyPress='return so_numero(event);'>
                  <?php echo form_error('rg');?>

                  <label for="descricao">Data de Nascimento</label>
                   <div id="datetimepicker4" class="input-append ">
                      <input data-format="dd/MM/yyyy" name="data" type="text" value="<?php echo $dados->data_nascimento; ?>"></input>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                        </i>
                      </span>
                   </div>
                   <?php echo form_error('data');?>

                  <label for="fname">Celular</label>
                  <input type="text"  id="celular" name="celular" value="<?php echo $dados->celular; ?>" maxlength="11" onKeyPress='return so_numero(event);'>
                  <?php echo form_error('celular');?>

                  <label for="fname">Nome da Mãe</label>
                  <input type="text"  id="nomem" name="nomem" value="<?php echo $dados->nomem; ?>" maxlength="100" onkeyup='maiuscula(this)'>
                  <?php echo form_error('nomem');?>

                  <label for="fname">Nome do Pai</label>
                  <input type="text"  id="nomep" name="nomep" value="<?php echo $dados->nomep; ?>" maxlength="100" onkeyup='maiuscula(this)'>
                  <?php echo form_error('nomep');?>

                  <label for="fname">Endereço</label>
                  <input type="text"  id="endereco" name="endereco" value="<?php echo $dados->ende; ?>" maxlength="100" onkeyup='maiuscula(this)'>
                  <?php echo form_error('endereco');?>


                    <p><br><br><br>

                      <button class="btn btn-green" type="submit">Editar</button>
                      <a href="<?php echo base_url(); ?>futebol/futeboladm" class="btn btn-red">Cancelar</a>
                    </p>
                  </div>  
                </div>
              </form>
            </div>
          </div>
          <div class="span4">
            <aside class="right-sidebar">
              <div class="widget">
                <h5 class="widgetheading">Menu Futebol</h5>
                <ul class="cat">
                  <?php foreach ($contadorAtletas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/futeboladm">Jogadores</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissao as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissao">Comissão Técnica</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorComissaoAntiga as $dado){?>   
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/comissaoAntiga">Comissão Técnica Antiga</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>
                  <?php foreach ($contadorAtletasAntigas as $dado){?>      
                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/AtletaAntigas">Jogadores Antigas</a><span> (<?php echo  $dado['contador'];?>)</span></li>
                  <?php }?>

                  <li><i class="icon-angle-right"></i><a href="<?php echo base_url(); ?>futebol/baixar">Baixar Planilha</a></li>

                </ul>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $(function() {
        $('#datetimepicker3').datetimepicker({
          pickDate: false
        });
      });
    </script>


    <script type="text/javascript">
  $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });
</script>

<script type="text/javascript">

  /* Atribui ao evento change do input FILE para upload da foto*/
var inputFile = document.getElementById("foto");
var foto_cliente = document.getElementById("foto-cliente");
if (inputFile != null && inputFile.addEventListener) {                   
    inputFile.addEventListener("change", function(){loadFoto(this, foto_cliente)});
} else if (inputFile != null && inputFile.attachEvent) {                  
    inputFile.attachEvent("onchange", function(){loadFoto(this, foto_cliente)});
}

/* Atribui ao evento change do input FILE para upload da foto*/
var inputFile = document.getElementById("foto_doc");
var foto_doc = document.getElementById("foto_documento");
if (inputFile != null && inputFile.addEventListener) {                   
    inputFile.addEventListener("change", function(){loadFoto(this, foto_doc)});
} else if (inputFile != null && inputFile.attachEvent) {                  
    inputFile.attachEvent("onchange", function(){loadFoto(this, foto_doc)});
}
/* Função para exibir a imagem selecionada no input file na tag <img>  */
function loadFoto(file, img){
    if (file.files && file.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
           img.src = e.target.result;
        }
        reader.readAsDataURL(file.files[0]);
    }
}


</script>
