 
<h1 class="text-center" style="text-align: center;"><img src="<?php echo base_url(); ?>assents/img/contrato.png" width="60" height="70"></h1>

<h5 class="text-center" style="text-align: center;"><strong>CONTRATO DE LICENÇA DE USO DE IMAGEM</strong></h5> 

<h5 class="text-center" style="text-align: center;"><strong>IDENTIFICAÇÃO DAS PARTES CONTRATANTES</strong></h5> 

<br><br>

<p style="font-size: 12px; text-align: justify;"><strong>LICENCIADO:</strong> LIONS FUTEBOL CLUBE – representado por Andréa Barbosa de Mélo, brasileira, solteira, pedagoga, nascida em 04 de novembro de 1966, inscrita no C.P.F. sob o  nº 461.858.104-25, residente e domiciliada na Rua da Mangabeira, nº 242, Bairro: Mangabeira, CEP: 52110-145, Cidade de Recife - Pernambuco;</p>

<p style="font-size: 12px; text-align: justify;"><strong>LICENCIANTE:</strong> <strong style="border-bottom: 1px solid; width: 80%;"><?php echo $dados[0]->nome ?></strong>, brasileiro(a), solteiro(a), nascido(a) em <strong style="border-bottom: 1px solid; width: 80%;"><?php echo $dia ?></strong> de <strong style="border-bottom: 1px solid; width: 80%;"><?php echo $mes?></strong> de <strong style="border-bottom: 1px solid; width: 80%;"><?php echo $ano ?></strong>, inscrito(a) no C.P.F. sob o  nº <strong style="border-bottom: 1px solid; width: 80%;"><?php echo $dados[0]->cpf ?></strong>.<br>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong><i>As partes acima identificadas têm, entre si, justo e acertado o presente Contrato de Licença de Uso de Imagem, que se regerá pelas cláusulas seguintes e pelas condições descritas no presente.</i></strong></p></p>

<p style="font-size: 12px; text-align: justify;"><strong>DO OBJETO DO CONTRATO</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 1ª.</strong> O presente CONTRATO tem como objeto a autorização, mediante licença, do uso de imagem do <strong>LICENCIANTE.</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Parágrafo primeiro.</strong> O <strong>LICENCIANTE</strong> declara ser o único detentor de todos os direitos patrimoniais e morais referentes à imagem cuja licença de uso é objeto do presente CONTRATO.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Parágrafo segundo.</strong> A licença concedida neste CONTRATO abrange somente o uso especificado nas cláusulas seguintes.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 2ª.</strong> As imagens licenciadas neste CONTRATO consistem em vídeos, fotografias, notícias em rede mundial de computadores (internet), entrevistas em jornais e revistas, todas relativas à atividade de futebol, não podendo ser reproduzidas imagens da vida privada do LICENCIANTE.</p>

<p style="font-size: 12px; text-align: justify;"><strong>DAS OBRIGAÇÕES DO LICENCIADO</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 3ª. O LICENCIADO</strong> se compromete a utilizar a imagem do <strong>LICENCIANTE</strong> somente para os seguintes fins específicos, veiculação em mídias de TV, JORNAL, WEB, MÍDIA EXTERIOR E MÍDIA GRÁFICA, CLUBES AGREMIAÇÕES, ENTIDADES ESPORTIVAS, EMPRESAS PARCEIRAS E DO RAMO ESPORTIVO EM GERAL.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 4ª.</strong> A presente licença autoriza o <strong>LICENCIADO</strong> a exibir as imagens para as mídias, clubes e empresas, acima citadas, tanto no território nacional, quanto no exterior.</p>
 
<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 5ª.</strong> O <strong>LICENCIADO</strong> poderá utilizar as imagens somente pelo prazo de 01 (um) ano, contados a partir da data da assinatura, como determinada na cláusula 9ª deste contrato.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 6ª.</strong> O <strong>LICENCIADO</strong> tem somente o direito do uso das imagens do LICENCIANTE para exibição nos moldes explicitados neste CONTRATO, não possuindo o direito de ceder ou vendê-las a terceiros, sem antes aceitação do LICENCIANTE.</p>

 <p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Parágrafo único.</strong> O <strong>LICENCIADO</strong> não se responsabiliza pelo uso indevido das imagens, cuja licença é objeto do presente instrumento, captadas por terceiros em exibições pelas mídias anteriores citadas. Todavia, a má utilização das imagens do LICENCIANTE decorridas da prestação de serviço do LICENCIADO, podem gerar indenização por danos, tanto morais, quanto materiais.</p>

<p style="font-size: 12px; text-align: justify;"><strong>DAS OBRIGAÇÕES DO LICENCIANTE</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 7ª.</strong> O <strong>LICENCIANTE</strong> se compromete a ceder sua imagem para utilização nos moldes desse contrato.</p>


<br><br><br><br><br>

<p style="font-size: 12px; text-align: justify;"><strong>DA EXCLUSIVIDADE</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 8ª.</strong> A imagem cedida pelo <strong>LICENCIANTE</strong>  será de utilização exclusiva do <strong>LICENCIADO</strong>, a quem competirá o direito de tomar as medidas judiciais e/ou extrajudiciais cabíveis para impedir a utilização da imagem por terceiros. </p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Parágrafo primeiro.</strong> O tempo da cessão com exclusividade será de 01 (um) ano, prorrogado por mais 1 ano, automaticamente caso não haja manifestação com mínimo de 30 dias por nenhuma das partes.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Parágrafo segundo.</strong> Esse artigo não inviabiliza a contratação de outros advogados para fazer cessar a veiculação da imagem do LICENCIANTE, com prévia liberação do LICENCIADO.</p>

<p style="font-size: 12px; text-align: justify;"><strong>DO PRAZO</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 09ª.</strong> O presente CONTRATO vigerá pelo prazo estipulado na cláusula 5ª.</p>

<p style="font-size: 12px; text-align: justify;"><strong>DA RESCISÃO</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 10ª.</strong> O contrato poderá ser rescindido por qualquer uma das partes, necessitando de prévia comunicação por escrito com pelo menos 30 (trinta) dias de antecedência.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Parágrafo primeiro.</strong> O desrespeito a qualquer cláusula deste contrato implicará em automática rescisão.</p>

<p style="font-size: 12px; text-align: justify;"><strong>DO FORO</strong></p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;"><strong>Cláusula 11ª.</strong> Para dirimir quaisquer controvérsias oriundas do <strong>CONTRATO</strong>, será competente o foro da Comarca de Recife/PE.</p>

<p style="text-indent: 30px; font-size: 12px; text-align: justify;">Por estarem assim justos e contratados, firmam o presente instrumento, em duas vias de igual teor.</p>
  
<p style="text-indent: 25px; font-size: 12px; text-align: justify;"> Recife, ________ de _________________ de <?php echo date('Y'); ?></p>

<br><br><br>

<center>
<div id="DivA"><p class="conteudo" style="font-size: 12px; text-align: justify;">________________________________________  

<?php  if ($dados[0]->status == 'Comissao') { ?>
	<br>COMISSÃO: <?php echo $dados[0]->nome ?>
<?php }else{ ?>
    <br>ATLETA: <?php echo $dados[0]->nome ?>
<?php } ?>
<br>CPF: <?php echo $dados[0]->cpf ?> </p></div>


<div id="DivB"><p style="font-size: 12px;">______________________________________________
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong style="font-size: 9px; width: 10px"><i>Andréa Barbosa de Mélo</i></strong>
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong style="font-size: 9px; text-align: center;"><i>Diretora LIONS F.C. </i></strong></p></div>
</center>
