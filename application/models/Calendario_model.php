<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendario_model extends CI_Model {
    
    
    public $tabela = 'calendario';
	public $chave  = 'id';


 public function consulta(){ 

    $date = date('Y-m-d'); 

    // var_dump($date); exit();

	$query ="SELECT 
				 calendario.`data_partida`,
				 calendario.`hora_partida`,
				 calendario.`local_partida`,
				 calendario.`torneio`,
				 calendario.`id_time_casa`,
				 calendario.`id_time_visitante`,
				 casa.`logo` AS logo_time_casa,
				 visitante.`logo`AS logo_time_visitante,
				 casa.`time` AS casa,
				 visitante.`time` AS visitante,
				 gol_time_casa,
				 gol_time_visitante,
				 penalti_time_casa,
				 penalti_time_visitante

				 FROM calendario 
				 
				 LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`
				 WHERE calendario.`gol_time_casa`IS NULL AND calendario.`gol_time_visitante` IS NULL
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC
				 LIMIT 6";



	  return $this->db->query($query)->result();

	}


	 public function consultaRR(){ 

    $date = date('Y-m-d'); 

    // var_dump($date); exit();

	$query ="SELECT 
				 calendario.`data_partida`,
				 calendario.`hora_partida`,
				 calendario.`local_partida`,
				 calendario.`torneio`,
				 calendario.`id_time_casa`,
				 calendario.`id_time_visitante`,
				 casa.`logo` AS logo_time_casa,
				 visitante.`logo`AS logo_time_visitante,
				 casa.`time` AS casa,
				 visitante.`time` AS visitante,
				 gol_time_casa,
				 gol_time_visitante,
				 penalti_time_casa,
				 penalti_time_visitante

				 FROM calendario 
				 
				 LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`
				 WHERE calendario.`gol_time_casa` IS NOT NULL AND calendario.`gol_time_visitante` IS NOT NULL
				 ORDER BY calendario.`data_partida` desc
				 LIMIT 6";



	  return $this->db->query($query)->result();

	}


	public function contadorProximasMenu()
	{
	$query ="SELECT COUNT(*) AS contador FROM `calendario`
	             LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`
				 WHERE calendario.`gol_time_casa`IS NULL AND calendario.`gol_time_visitante` IS NULL
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC";

	  return $this->db->query($query)->result();

	}

  public function contadorRealizadasMenu()
	{
	$query ="SELECT COUNT(*) AS contador FROM `calendario`
	             LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`
				 WHERE calendario.`gol_time_casa` IS NOT NULL AND calendario.`gol_time_visitante` IS NOT NULL
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC";

	  return $this->db->query($query)->result();

	}


	public function contadorResultadosMenu()
	{
	$query ="SELECT COUNT(*) AS contador FROM `calendario`
	             LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC";

	  return $this->db->query($query)->result();

	}

    public function contadorTimesMenu()
	{
	$query ="SELECT COUNT(*) AS contador FROM `times`";

	  return $this->db->query($query)->result();

	}

  public function times()
	{
	  $query ="SELECT * FROM `times`";

	  return $this->db->query($query)->result();

	}

  public function cadastrarPartidasExe($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if($this->db->affected_rows() == '1')
	    	{
	    	return true;
	    	}
	    	return false;

	}

  public function editarProximas($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

  public function editarProximasExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

  public function excluirProximas($dados)
	{
        //var_dump($dados);
      $this->db->delete($this->tabela, array('id' => $dados['id'])); 
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

  public function editarRealizadas($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

  public function editarRealizadasExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

   public function excluirRealizadas($dados)
	{
        //var_dump($dados);
      $this->db->delete($this->tabela, array('id' => $dados['id'])); 
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

 public function editarResultados($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}


  public function editarResultadosExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

	 public function editarTimes($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get('times')->result();
	}


	  public function editarTimesExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update('times',$dados))
		{
			return true;
		}

		return false;

	}

	  public function adicionarTimesExe($dados)
	{
		$this->db->insert('times', $dados);

		if($this->db->affected_rows() == '1')
	    	{
	    	return true;
	    	}
	    	return false;

	}

	public function ListarProximas($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (visitante.`time` LIKE '%".$pesquisa."%' OR casa.`time` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT 
		         calendario.`id`,
				 calendario.`data_partida`,
				 calendario.`hora_partida`,
				 calendario.`local_partida`,
				 calendario.`torneio`,
				 calendario.`id_time_casa`,
				 calendario.`id_time_visitante`,
				 casa.`logo` AS logo_time_casa,
				 visitante.`logo`AS logo_time_visitante,
				 casa.`time` AS casa,
				 visitante.`time` AS visitante,
				 gol_time_casa,
				 gol_time_visitante,
				 penalti_time_casa,
				 penalti_time_visitante

				 FROM calendario 
				 
				 LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`

		         WHERE calendario.`gol_time_casa`IS NULL AND calendario.`gol_time_visitante` IS NULL ".$queryPesquisa." 
		         ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

    public function contadorProximas($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (visitante.`time` LIKE '%".$pesquisa."%' OR casa.`time` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `calendario`
	             LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id` 
				 WHERE calendario.`gol_time_casa`IS NULL AND calendario.`gol_time_visitante` IS NULL ".$queryPesquisa." 
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC ";
				

				return $this->db->query($query)->result();

	 }


	 public function ListarRealizadas($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (visitante.`time` LIKE '%".$pesquisa."%' OR casa.`time` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT 
		         calendario.`id`,
				 calendario.`data_partida`,
				 calendario.`hora_partida`,
				 calendario.`local_partida`,
				 calendario.`torneio`,
				 calendario.`id_time_casa`,
				 calendario.`id_time_visitante`,
				 casa.`logo` AS logo_time_casa,
				 visitante.`logo`AS logo_time_visitante,
				 casa.`time` AS casa,
				 visitante.`time` AS visitante,
				 gol_time_casa,
				 gol_time_visitante,
				 penalti_time_casa,
				 penalti_time_visitante

				 FROM calendario 
				 
				 LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`

		         WHERE calendario.`gol_time_casa` IS NOT NULL AND calendario.`gol_time_visitante` IS NOT NULL ".$queryPesquisa." 
		         ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

	    public function contadorRealizadas($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (visitante.`time` LIKE '%".$pesquisa."%' OR casa.`time` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `calendario`
	             LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id` 
				 WHERE calendario.`gol_time_casa` IS NOT NULL AND calendario.`gol_time_visitante` IS NOT NULL ".$queryPesquisa." 
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC ";
				

				return $this->db->query($query)->result();

	 }


	 	 public function resultados($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (visitante.`time` LIKE '%".$pesquisa."%' OR casa.`time` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT 
		         calendario.`id`,
				 calendario.`data_partida`,
				 calendario.`hora_partida`,
				 calendario.`local_partida`,
				 calendario.`torneio`,
				 calendario.`id_time_casa`,
				 calendario.`id_time_visitante`,
				 casa.`logo` AS logo_time_casa,
				 visitante.`logo`AS logo_time_visitante,
				 casa.`time` AS casa,
				 visitante.`time` AS visitante,
				 gol_time_casa,
				 gol_time_visitante,
				 penalti_time_casa,
				 penalti_time_visitante

				 FROM calendario 
				 
				 LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id`

		         WHERE  calendario.`id` IS NOT NULL".$queryPesquisa." 
		         ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

	    public function contadorResultados($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (visitante.`time` LIKE '%".$pesquisa."%' OR casa.`time` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `calendario`
	             LEFT JOIN `times` casa
				 ON calendario.`id_time_casa` = casa.`id`
				 
				 LEFT JOIN `times` visitante
				 ON calendario.`id_time_visitante` = visitante.`id` 
				 WHERE  calendario.`id` IS NOT NULL ".$queryPesquisa." 
				 ORDER BY calendario.`data_partida`, calendario.`hora_partida` ASC ";
				

				return $this->db->query($query)->result();

	 }


	 	 public function listarTimes($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (times.`time` LIKE '%".$pesquisa."%' OR times.`time` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `times`

		         WHERE  `times`.`id` IS NOT NULL".$queryPesquisa." 
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

	    public function contadorTimes($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (times.`time` LIKE '%".$pesquisa."%' OR times.`time` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `times`

				 WHERE   `times`.`id` IS NOT NULL ".$queryPesquisa." ";
				

				return $this->db->query($query)->result();

	 }



}