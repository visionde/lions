<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public $tabela = 'login';
	public $chave  = 'id';


   public function contadorUsuarios()
	{
	$query ="SELECT COUNT(*) AS contador FROM `login`
	         WHERE visivel = 1";

	  return $this->db->query($query)->result();

	}


    public function Listar($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (nome LIKE '%".$pesquisa."%' OR nome LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `login` JOIN `perfil` ON `perfil`.`perfil_id` = `login`.`nivel` 
		WHERE `visivel` = 1 ".$queryPesquisa." 
        LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";
		return $this->db->query($query)->result();

	}

	public function contador($page, $pesquisa = NULL)
	 {

	    $queryPesquisa = !is_null($pesquisa) ? " AND (titulo LIKE '%".$pesquisa."%' OR titulo LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `noticias` 
					WHERE visivel = 1 ".$queryPesquisa." ";
					return $this->db->query($query)->result();

	 }

   public function cadastrarUsuarioExe($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if($this->db->affected_rows() == '1')
	    	{
	    	return true;
	    	}
	    	return false;

	}


}