<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias_model extends CI_Model {

	public $tabela = 'noticias';
	public $chave  = 'id';


 public function consultaNoticias(){ 

	$query ="SELECT * FROM noticias
	        WHERE visivel = 1
			ORDER BY noticias.`data_noticias` DESC
			LIMIT 8";

	  return $this->db->query($query)->result();

	}

   public function consultaNoticiasPrincipal(){ 

	$query ="SELECT * FROM noticias
	        WHERE visivel = 1
			ORDER BY noticias.`data_noticias` DESC
			LIMIT 5";

	  return $this->db->query($query)->result();

	}


	public function consultaNoticiasVisualizacao($id){ 
		// var_dump($id); exit();

	$query ="SELECT * FROM noticias
             WHERE noticias.`id`= '".$id."'
             AND visivel = 1";

	  return $this->db->query($query)->result();

	}

    public function cadastrarNoticiasExe($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if($this->db->affected_rows() == '1')
	    	{
	    	return true;
	    	}
	    	return false;

	}

   public function contadorNoticias()
	{
	$query ="SELECT COUNT(*) AS contador FROM `noticias`
	         WHERE visivel = 1";

	  return $this->db->query($query)->result();

	}


  public function Listar($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (titulo LIKE '%".$pesquisa."%' OR titulo LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `noticias`
		WHERE visivel = 1 ".$queryPesquisa." 
        LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";
		return $this->db->query($query)->result();

	}

public function contador($page, $pesquisa = NULL)
  {

    $queryPesquisa = !is_null($pesquisa) ? " AND (titulo LIKE '%".$pesquisa."%' OR titulo LIKE '%".$pesquisa."%')" : "";

	  $query =  "SELECT COUNT(*) AS total FROM `noticias` 
				WHERE visivel = 1 ".$queryPesquisa." ";
				return $this->db->query($query)->result();

  }

public function editar($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

public function editarExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function excluir($dados,$id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}
	


}