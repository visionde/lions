<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Futebol_model extends CI_Model {
    
    
    public $tabela = 'jogadoras';
	public $chave  = 'id';


 // public function consultaElencoAtacante(){ 

	// $query ="SELECT * FROM jogadoras
 //             WHERE jogadoras.`status` = 'Atleta' AND jogadoras.`visivel` = '1' ";

	//   return $this->db->query($query)->result();

	// }



 public function consultaComissao(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`status` = 'Comissao'
             AND jogadoras.`visivel` = '1' 
             AND (jogadoras.`time` = 'F'  OR  jogadoras.`time` = 'A')";

	  return $this->db->query($query)->result();

	}


 public function consultaComissaoM(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`status` = 'Comissao'
             AND jogadoras.`visivel` = '1' 
             AND (jogadoras.`time` = 'M'  OR  jogadoras.`time` = 'A')";

	  return $this->db->query($query)->result();

	}

####### INICIO FEMININO #########
 
 public function consultaElencoGoleira(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Goleira' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'F' ";
	  return $this->db->query($query)->result();

	}
 public function consultaElencoDefensoras(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Defensora' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'F' ";

	  return $this->db->query($query)->result();

	}

 public function consultaElencoMeia(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Meia' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'F' ";

	  return $this->db->query($query)->result();

	}
 public function consultaElencoAtacante(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Atacante' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'F' ";

	  return $this->db->query($query)->result();

	}

######### FIM FEMININO ########

####### INICIO MASCULINO #########
 
 public function consultaElencoGoleiro(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Goleiro' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'M' ";
	  return $this->db->query($query)->result();

	}
 public function consultaElencoDefensores(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Defensor' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'M' ";

	  return $this->db->query($query)->result();

	}

 public function consultaElencoMeiaM(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Meia' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'M' ";

	  return $this->db->query($query)->result();

	}
 public function consultaElencoAtacanteM(){ 

	$query ="SELECT * FROM jogadoras
             WHERE jogadoras.`posicao` = 'Atacante' AND jogadoras.`visivel` = '1' AND  jogadoras.`time` = 'M' ";

	  return $this->db->query($query)->result();

	}

######### FIM MASCULINO ########


 public function contadorAtletasMenu()
	{

	$query ="SELECT COUNT(*) AS contador FROM jogadoras
         WHERE jogadoras.`status` = 'Atleta' AND jogadoras.`visivel` = '1' ";

	  return $this->db->query($query)->result();

	}

  public function contadorComissaoMenu()
	{

	$query ="SELECT COUNT(*) AS contador FROM jogadoras
         WHERE jogadoras.`status` = 'Comissao' AND jogadoras.`visivel` = '1' ";

	  return $this->db->query($query)->result();

	}

  public function contadorComissaoAntigaMenu()
	{

	$query ="SELECT COUNT(*) AS contador FROM jogadoras
         WHERE jogadoras.`status` = 'Comissao' AND jogadoras.`visivel` = '0' ";

	  return $this->db->query($query)->result();

	}

  public function contadorAtletasAntigasMenu()
	{

	$query ="SELECT COUNT(*) AS contador FROM jogadoras
         WHERE jogadoras.`status` = 'Atleta' AND jogadoras.`visivel` = '0' ";

	  return $this->db->query($query)->result();

	}

  public function listarAtletas($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `jogadoras`

		         WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '1' AND jogadoras.`status` = 'Atleta' ".$queryPesquisa."
		         ORDER BY `jogadoras`.`nome`
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

  public function contadorAtletas($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `jogadoras`

				 WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '1' AND jogadoras.`status` = 'Atleta' ".$queryPesquisa." ";
				

				return $this->db->query($query)->result();

	 }

  public function adicionarAtletaExe($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if($this->db->affected_rows() == '1')
	    	{
	    	return true;
	    	}
	    	return false;

	}

 public function editarAtleta($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

 public function editarAtletaExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function excluirAtleta($dados,$id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

 public function listarComissao($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `jogadoras`

		         WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '1' AND jogadoras.`status` = 'Comissao' ".$queryPesquisa."
		         ORDER BY `jogadoras`.`nome`
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

  public function contadorComissao($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `jogadoras`

				 WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '1' AND jogadoras.`status` = 'Comissao' ".$queryPesquisa." ";
				

				return $this->db->query($query)->result();

	 }

 public function listarComissaoAntiga($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `jogadoras`

		         WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '0' AND jogadoras.`status` = 'Comissao' ".$queryPesquisa."
		         ORDER BY `jogadoras`.`nome`
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

  public function contadorComissaoAntiga($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `jogadoras`

				 WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '0' AND jogadoras.`status` = 'Comissao' ".$queryPesquisa." ";
				

				return $this->db->query($query)->result();

	 }

  public function adicionarComissaoExe($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if($this->db->affected_rows() == '1')
	    	{
	    	return true;
	    	}
	    	return false;

	}

 public function editarComissao($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

 public function editarComissaoExe($dados,$id)
	{			
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;

	}

 public function excluirComissao($dados,$id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

 public function listarAtletaAntigas($page, $itens_per_page, $pesquisa = NULL)
	{

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		$query ="SELECT * FROM `jogadoras`

		         WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '0' AND jogadoras.`status` = 'Atleta' ".$queryPesquisa."
		         ORDER BY `jogadoras`.`nome`
                 LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";


		return $this->db->query($query)->result();

	}

  public function contadorAtletaAntigas($page, $pesquisa = NULL)
	 {

       $queryPesquisa = !is_null($pesquisa) ? " AND (jogadoras.`nome` LIKE '%".$pesquisa."%' OR jogadoras.`apelido` LIKE '%".$pesquisa."%')" : "";

		  $query =  "SELECT COUNT(*) AS total FROM `jogadoras`

				 WHERE  `jogadoras`.`id` IS NOT NULL AND `jogadoras`.`visivel` = '0' AND jogadoras.`status` = 'Atleta' ".$queryPesquisa." ";
				

				return $this->db->query($query)->result();

	 }


 public function ativarAtletaAntiga($dados,$id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

  public function ativarComissaoAntiga($dados,$id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

 public function vizualizarAtletaAntiga($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

 public function vizualizarComissaoAntiga($dados)
	{
		$this->db->select('*');
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();
	}

 public function listarExecel($time, $tipo, $status)
	{
		$this->db->select('*');
		$this->db->where('jogadoras.time', $time);
		$this->db->where('jogadoras.`visivel`', $tipo);
		$this->db->where('jogadoras.`status`', $status);
		return $this->db->get($this->tabela)->result();
	}


}